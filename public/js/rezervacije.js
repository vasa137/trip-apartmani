
function areYouSure(){
    if (confirm('Da li ste sigurni da zelite da obrišete rezervaciju?')) {
        return true;
    } else {
        return false;
    }
}

function validirajDatume(){
    let dateStart = document.getElementById('reservation-date-start');
    let dateEnd = document.getElementById('reservation-date-end');

    let d1 = new Date(dateStart.value);
    let d2 = new Date(dateEnd.value);

    console.log(d1, d2);

    if(d1.getTime() < d2.getTime()){
        return true;
    }
    else{
        alert('Datum dolaska mora biti pre datuma odlaska!')
        return false;
    }
}