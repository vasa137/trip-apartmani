let countArr = 1, countTable = 1;
let lastMaxPeople = -1;
let imagesToDel = [];
function setCounters(tipCene, brojNoci){
    let priceArr = document.getElementById('price-arr');
    let priceTab = document.getElementById('price-tab');

    if(tipCene == 'niz'){
        countArr = brojNoci;

        priceArr.style.display = 'inherit';
        $('#price-arr').find('input').attr('required', 'required');

        priceTab.style.display = 'none';
        $('#price-tab').find('input').removeAttr('required');
    }
    else{
        countTable = brojNoci;

        priceTab.style.display = 'inherit';
        $('#price-tab').find('input').attr('required', 'required');

        priceArr.style.display = 'none';
        $('#price-arr').find('input').removeAttr('required');
    }

    $('#main_image').removeAttr('required');
    $('#filer_input1').removeAttr('required');
}

function setLastMaxPeople(lmp){
    lastMaxPeople = lmp;
}

function appendInputsImgToDel(){
    $toDelDiv = $('#toDelDiv');

    for( let i = 0; i < imagesToDel.length; i++){
        $toDelDiv.append('<input type="hidden" name="toDel[]" value="' + imagesToDel[i] + '"/>')
    }

    return true;
}

function previewImage(input){
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function(e) {
            $('#main_img_prev').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function removeImg(slika,id){
    let paragr = document.getElementById(id);
    if(paragr.innerHTML === ''){
        paragr.innerHTML = "Slika registrovana za brisanje";
        imagesToDel.push(slika);
    }
    else{
        paragr.innerHTML = '';
        for( let i = 0; i < imagesToDel.length; i++){
            if ( imagesToDel[i] === slika) {
                imagesToDel.splice(i, 1);
            }
        }
    }


}

function typePriceRadioClicked(i){

    let priceArr = document.getElementById('price-arr');
    let priceTab = document.getElementById('price-tab');

    // ukloni gresku
    let errorRadio = document.getElementById('error-radio-price');
    errorRadio.style.display = 'none';

    // niz
    if(i === 1){
        priceArr.style.display = 'inherit';
        $('#price-arr').find('input').attr('required', 'required');

        priceTab.style.display = 'none';
        $('#price-tab').find('input').removeAttr('required');
    }
    // tabela
    else{
        let maxPeople = document.getElementById('property-max-people');
        if(isNaN(maxPeople.value) || maxPeople.value <= 0){
            let arrayRadio = document.getElementById('property-price-array');
            arrayRadio.checked = true;
            errorRadio.style.display = 'inherit'; // prikazi gresku
        }
        else {

            resetTable(maxPeople.value);

            priceTab.style.display = 'inherit';
            $('#price-tab').find('input').attr('required', 'required');

            priceArr.style.display = 'none';
            $('#price-arr').find('input').removeAttr('required');
        }
    }
}

function resetTable(maxPeopleVal){
    if(lastMaxPeople === -1 || lastMaxPeople !== maxPeopleVal){
        countTable = 1;

        let priceTabContent = document.getElementById('price-tab-content');
        priceTabContent.innerHTML = getPriceTabRow(countTable);
        $tableCombination = $('#table-combinations-' + countTable);

        for(let i = 1; i <= maxPeopleVal; i++)
        {
            $tableCombination.append(getPriceTabRowCombination(countTable, i));
        }

        lastMaxPeople = maxPeopleVal;

        let row1ButtonX = document.getElementById('tab-button-x-' + countTable);

        row1ButtonX.disabled = true;
    }
}

function checkRadiosMaxPeople(val){
    let arrayRadio = document.getElementById('property-price-array');
    // ako je cekirana tabela - resetuj je
    if(arrayRadio.checked === false){
        resetTable(val);
    }
}

//apenduj sa jQueryjem innerHTML brise values-e

function addPriceArrayRow(){
    countArr++;

    $priceArrContent = $('#price-arr-content');

    $priceArrContent.append(getPriceArrRow(countArr));

    let lastRowButtonX = document.getElementById('arr-button-x-' + (countArr - 1));
    lastRowButtonX.disabled = true;
}

function addPriceTableRow(){
    countTable++;

    $priceTableContent = $('#price-tab-content');
    $priceTableContent.append(getPriceTabRow(countTable));

    let tableCombination = document.getElementById('table-combinations-' + countTable);

    for(let i = 1; i <= lastMaxPeople; i++)
    {
        tableCombination.innerHTML += getPriceTabRowCombination(countTable, i);
    }

    let lastRowButtonX = document.getElementById('tab-button-x-' + (countTable - 1));
    lastRowButtonX.disabled = true;
}

function removeTabRow(){
    let priceTabRow = document.getElementById('price-tab-row-' + countTable);
    priceTabRow.remove();

    countTable--;

    if(countTable > 1){
        let lastRowButtonX = document.getElementById('tab-button-x-' + countTable);
        lastRowButtonX.disabled = false;
    }
}

function removeArrRow(){
    let priceArrRow = document.getElementById('price-arr-row-' + countArr);
    priceArrRow.remove();

    countArr--;

    if(countArr > 1){
        let lastRowButtonX = document.getElementById('arr-button-x-' + countArr);
        lastRowButtonX.disabled = false;
    }
}

function getPriceArrRow(i){
    return '<div id="price-arr-row-' + i + '" class="row">\n' +
        '                                                    <div class="col-sm-5 m-b-10">\n' +
        '                                                        <label for="arr-price-nights-' + i + '">Broj noćenja</label>\n' +
        '                                                        <input name="arr-price-nights[]" type="number" min="1" step="1" class="form-control" id="arr-price-nights-' + i + '" required>\n' +
        '                                                    </div>\n' +
        '                                                    <div class="col-sm-6 m-b-10">\n' +
        '                                                        <label for="arr-price-val-'+ i + '">Cena (eur)</label>\n' +
        '                                                        <input name="arr-price-vals[]" type="number" min="1" step="any" class="form-control" id="arr-price-val-'+ i + '" required>\n' +
        '\n' +
        '                                                    </div>\n' +
        '                                                    <div class="col-sm-1 m-b-10">\n' +
        '                                                        <br/>\n' +
        '                                                        <button id="arr-button-x-' + i + '" onclick="removeArrRow()" class="btn btn-icon waves-effect waves-light btn-danger"> <i class="fa fa-remove"></i> </button>\n' +
        '                                                    </div>\n' +
        '                                                </div>';
}

function getPriceTabRow(i){
    return '<div class="form-group"  id="price-tab-row-'  + i +'">\n' +
        '\n' +
        '                                                    <div class="row">\n' +
        '                                                        <div class="col-sm-5 m-b-10">\n' +
        '                                                            <label for="price-nights-' + i + '">Broj noćenja</label>\n' +
        '                                                            <input name="tab-price-nights[]" type="number" min="1" step="1" class="form-control" id="price-nights-' + i + '" required>\n' +
        '                                                        </div>\n' +
        '\n' +
        '                                                        <div class="col-sm-1 m-b-10">\n' +
        '                                                            <br/>\n' +
        '                                                            <button id="tab-button-x-' + i + '" class="btn btn-icon waves-effect waves-light btn-danger" onclick="removeTabRow()"> <i class="fa fa-remove"></i> </button>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                    <br/>\n' +
        '                                                    <div id="table-combinations-'  + i + '">\n' +
        '                                                    </div>\n</div>';
}

// i - redni broja nocenja, j - redni broj kombinacije za i-ti broj nocenja

function getPriceTabRowCombination(i, j){
   return '<div class="row" id="table-comb-row-' + i + '-' + j + '">\n' +
    '                                                            <div class="col-sm-6 m-b-10">\n' +
    '                                                                <label for="price-people-' + i + '-' + j + '">Broj ljudi</label>\n' +
    '                                                                <input readonly name="tab-price-people[' + i + '][]"' +' type="number" min="1" step="1" class="form-control" id="price-people-' + i  +'-' + j + '" value="' + j + '" required>\n' +
    '                                                            </div>\n' +
    '                                                            <div class="col-sm-6 m-b-10">\n' +
    '                                                                <label for="price-nights-val-' + i + '-' + j + '">Cena (eur)</label>\n' +
    '                                                                <input name="tab-price-vals[' + i +'][]" type="number" min="1" step="any" class="form-control" id="price-nights-val-' + i  +'-' + j + '" required>\n' +
    '                                                            </div>\n' +
    '                                                        </div>\n';
}

function areYouSure(){
    if (confirm('Da li ste sigurni da zelite da obrišete stan?')) {
        return true;
    } else {
        return false;
    }
}