@extends('layout')

@section('title','Pitanja')

@section('act_pitanja')
class="act-link"
@stop

@section('glavni_sadrzaj')
 <div id="wrapper">
                <!--content -->  
                <div class="content">
                    
                    
                    
                    
        <!--section -->  
        <section class="gray-bg" id="sec4">
            <div class="container">
                <div class="section-title">
                    <h2> Često postavljana pitanja</h2>
                    <div class="section-subtitle">Često postavljana pitanja</div>
                    <span class="section-separator"></span>
                    <p>Za sva dodatna pitanja, molimo vas da nas kontaktirate putem e-maila, ili pozivom na telefon.</p>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="accordion">
                            <a class="toggle " > NA KOJI NAČIN MOGU REZERVISATI APARTMAN?  <i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Apartman možete rezervisati popunjavanjem formulara u odeljku REZERVIŠI na našem sajtu ili pozivom na telefon. Vaša rezervacija postaje validna onog momenta kada od nas dobijete potvrdu rezervacije putem elektronske pošte ili telefona.</p>
                            </div>
                            <a class="toggle " > DA LI JE POTREBNO AVANSNO PLAĆANJE?<i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>U roku od 48 časova nakon rezervacije potrebno je uplatiti avans u visini od 30% od ukupnog iznosa rezervacije. Preostali iznos plaća se po dolasku, prilikom preuzimanja ključeva. Kada primimo vašu uplatu mi ćemo vam putem elektronske pošte poslati potvrdu rezervacije sa svim potrebnim informacijama. Ako ne primimo uplatu u roku od 48 sati vaša rezervacija će biti otkazana, a apartman će biti stavljen na raspolaganje drugim gostima. </p>
                            </div>
                            <a class="toggle " >KOJI SU MOGUĆI NAČINI PLAĆANJA? <i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Smeštaj možete platiti gotovinom prilikom ulaska u apartman, ili avansno putem bankovnog transfera.</p>
                            </div>
                            <a class="toggle " >KADA JE PREDVIĐENO VREME ULASKA U APARTMAN, A KADA IZLASKA IZ NJEGA?<i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Predviđeno vreme ulaska u apartman je od 14 časova. Predviđeno vreme izlaska iz apartmana je do 11 sati.</p>
                            </div>
                            <a class="toggle " >GDE TREBA DA PREUZMEM KLJUČEVE? <i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Ključeve od apartmana, mapu grada i sve dodatne informacije dobićete od domaćina prilikom ulaska u apartman.</p>
                            </div>
                           

                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="accordion">
                        	 <a class="toggle " >DA LI JE POTREBNO OSTAVITI SIGURNOSNI DEPOZIT?<i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Trip-Apartmani zadržavaju pravo da naplate sigurnosni depozit u iznosu od 100 evra prilikom ulaska gostiju u apartman. Taj iznos biće u potpunosti vraćen pod uslovom da u apartmanu nije načinjena nikakva šteta. Provera invertara vrši se prilikom napuštanja apartmana.</p>
                            </div>
                             <a class="toggle" >DA LI JE MOGUĆE PRIJAVLJIVANJE I ODJAVLJIVANJE IZ APARTAMANA U RANIM ILI KASNIM SATIMA? <i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Trip-Apartmani su fleksibilni po pitanju prijavljivanja i odjavljivanja iz apartmana dok god to ne utiče na druge goste. Molimo vas da nas unapred obavestite o vremenu vašeg dolaska.</p>
                            </div>
                            <a class="toggle" >DA LI NUDITE TRENSFER OD AERODROMA I DO NJEGA?<i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Da, mi vam možemo organizovati transfer na vaš zahtev po ceni od 20 evra po jednom pravcu.</p>
                            </div>
                             <a class="toggle" >KAKO MOGU DA PROMENIM REZERVACIJU? <i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>Za bilo kakve promene možete nas pozvati telefonom ili nam poslati e-mail s pozivom na broj rezervacije.</p>
                            </div>
                             <a class="toggle" >KAKVA SU PRAVILA PRILIKOM OTKAZIVANJA?<i class="fa fa-angle-down"></i></a>
                            <div class="accordion-inner ">
                                <p>U slučaju otkazivanja rezervacije, molimo vas da nas blagovremeno obavestite. Za avansne uplate ne vršimo povraćaj novca. Zahvaljujemo se na razumevanju.</p>
                            </div>
                        </div>
                    </div>
                </div>
                 
            </div>
        </section>
        <!-- section end -->
       
        <div class="limit-box"></div>
    </div>
    <!-- content end -->
</div>
<!-- wrapper end -->
@stop