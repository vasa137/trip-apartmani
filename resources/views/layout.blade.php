<!DOCTYPE HTML>
<html lang="en">
    <head>
       <link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css')}}">
     
 
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Trip-Apartmani - @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="{{asset('css/reset.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/plugins.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/color.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/general.css')}}">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">




        @yield('skripte')
        <style>
        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
          padding-left: 10px;
          padding-right: 10px;
          padding-top: 5px;
          padding-bottom: 5px;
          text-align: right;
        }

        </style>
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>

        <h2>Modal Example</h2>
                  <!-- Trigger the modal with a button -->
                  
                
                 @yield('cenovnik')


        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->
            <header class="main-header dark-header fs-header sticky">
                <div class="header-inner">
                    <div class="logo-holder">
                        <a href="/"><img style="height: 150%" src="{{asset('images/logo.png')}}" alt=""></a>
                    </div>
                    
                    <div class="header-search vis-header-search">
                        <p class="header-search-button" >
                            <a style="color:white" href="tel:+381611947722">
                                (+381) 061-194-77-22
                            </a> , 
                            <a style="color:white" href="tel:+381641411900">
                                064-14-11-900
                            </a>
                            </p>
                    </div>
                   
                    <a style="padding:10px 30px;" href="/kontakt" class="add-list btn  big-btn circle-btn  dec-btn color-bg flat-btn ">Kontakt <span><i class="fa fa-phone"></i></span></a>
                    
                    <!-- nav-button-wrap-->
                    <div class="nav-button-wrap color-bg">
                        <div class="nav-button">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                    <!-- nav-button-wrap end-->
                    <!--  navigation -->
                    <div class="nav-holder main-menu">
                        <nav>
                            <ul>
                                <!--
                                <li>
                                    <a href="/" @yield('act_naslovna')>NASLOVNA</a> 
                                </li>
                                -->
                                <li>
                                    <a href="/stanovi" @yield('act_stanovi')>APARTMANI</a>
                                </li>
                                <li>
                                    <a href="/mapa-stanova" @yield('act_mapa_stanova')>MAPA APARTMANA</a>
                                </li>
                                <li>
                                    <a href="/kako-rezervisati" @yield('act_kako')>KAKO REZERVISATI?</a>
                                </li>
                               
                                <li>
                                    <a href="/pitanja" @yield('act_pitanja')>ČESTA PITANJA</a>
                                </li>
                                <li>
                                    <a href="#"><img style="height: 50%" src="{{asset('images/srpski.png')}}" alt=""></a>
                                    <a href="#"><img style="height: 50%" src="{{asset('images/engleski.png')}}" alt=""></a>
                                </li>

                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                </div>
            </header>
            <!--  header end -->
            <!--  wrapper  -->
            @yield('glavni_sadrzaj')

            <!-- wrapper end -->
            <!--footer -->
            <footer class="main-footer dark-footer  ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-widget fl-wrap">
                                <h3>O nama</h3>
                                <div class="footer-contacts-widget fl-wrap">
                                    <p>AKO TRAŽITE UDOBAN I POVOLJAN SMEŠTAJ U CENTRU BEOGRADA, NA PRAVOM STE MESTU! ODABERITE APARTMAN NA PRESTIŽNIM LOKACIJAMA U CENTRU BEOGRADA.  </p>
                                    <ul  class="footer-contacts fl-wrap">
                                        <li><span><i class="fa fa-envelope-o"></i></span><a href="#" target="_blank">info@trip-apartmani.com</a></li>
                                        
                                        <li><span><i class="fa fa-phone"></i>  </span><a href="tel:+381611947722">(+381) 061-194-77-22</a></li>
                                         <li><span><i class="fa fa-phone"></i>  </span><a href="tel:+381641411900">(+381) 064-14-11-900</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="footer-widget fl-wrap">
                                <h3>Apartmani</h3>
                                <div class="widget-posts fl-wrap">
                                    <ul>
                                        <li class="clearfix">
                                            <a href="/stan/Shining-stan-na-dan-beograd/16"  class="widget-posts-img"><img src="/images/stanovi/16/glavna.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="/stan/Shining-stan-na-dan-beograd/16" title="">Shining</a>
                                                <span class="widget-posts-date">Trosoban apartman sa garažom u centru Beograda, u Budimskoj ulici na Dorćolu.</span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="/stan/Sveti Sava-stan-na-dan-beograd/15"  class="widget-posts-img"><img src="/images/stanovi/15/glavna.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="/stan/Sveti Sava-stan-na-dan-beograd/15" title="">Sveti Sava</a>
                                                <span class="widget-posts-date">Studio-apartman u centru Beograda, u Ulici Svetog Save na Vračaru.</span>
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="/stan/Krunska 54-stan-na-dan-beograd/18"  class="widget-posts-img"><img src="/images/stanovi/18/glavna.jpg" class="respimg" alt=""></a>
                                            <div class="widget-posts-descr">
                                                <a href="/stan/Krunska 54-stan-na-dan-beograd/18" title="">Krunska 54</a>
                                                <span class="widget-posts-date">Nov, trosoban apartman s parking mestom u centru Beograda.</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="footer-widget fl-wrap">
                                <!--
                                <h3>Subscribe</h3>
                                <div class="subscribe-widget fl-wrap">
                                    <p>Ili bilo koja druga opcija, npr. kontaktirajte nas, proverite raspolozivost...</p>
                                    <div class="subcribe-form">
                                       
                                        
                                            <a href="/kontakt" id="subscribe-button" class="subscribe-button"><i class="fa fa-phone"></i> Kontakt</a>
                                            
                                       
                                    </div>
                                </div>
                                -->
                                <h3>Opcije</h3>
                                <div class="subcribe-form">


                                    <a href="/kontakt" id="subscribe-button" class="subscribe-button"><i class="fa fa-phone"></i> Kontakt</a>


                                </div>

                                <div class="footer-widget fl-wrap">
                                    <div class="footer-menu fl-wrap">
                                        <ul>
                                            <li><a href="/">Naslovna </a></li>
                                            <li><a href="/stanovi">Apartmani</a></li>
                                            <li><a href="/mapa-stanova">Mapa apartmana</a></li>
                                            <li><a href="/kontakt">Kontakt</a></li>
                                            <li><a href="/pitanja">Pitanja i odgovori</a></li>
                                            <li><a href="/koraci">Kako rezervisati</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sub-footer fl-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="about-widget">
                                    <img src="{{asset('images/logo.png')}}" alt="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="copyright"> &#169; Trip-Apartmani 2019 .  Sva prava zadržana.</div>
                            </div>
                            <div class="col-md-4">
                                <div class="footer-social">
                                    <ul>
                                        <a href="https://www.instagram.com/tripapartmani/" target="_blank"><img style="height: 25px;" src="{{asset('images/insta.png')}}"></a>
                                        <a href="https://www.facebook.com/tripapartmani/?ref=br_rs" target="_blank"><img style="height: 25px;" src="{{asset('images/fb.png')}}"></a>
                                        <a href="https://twitter.com/ApartmaniTrip " target="_blank"><img style="height: 25px;" src="{{asset('images/twitter.png')}}"></a>
                                        <a href="whatsapp://send?abid=[users name]&text=[message text]" target="_blank"><img style="height: 25px;" src="{{asset('images/wa.png')}}"></a>
                                        <a href="viber://pa?chatURI=[public account URI]&text=[message text]" target="_blank"><img style="height: 25px;" src="{{asset('images/viber.png')}}"></a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--footer end  -->
            
            <a class="to-top"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->

    </body>
         <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCDJGMbgsHdxyy8_5H-hgHqoPUgXVj740&libraries=places&callback=initAutocomplete"></script>
    
        
        <script type="text/javascript" src="{{asset('js/map_infobox.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/markerclusterer.js')}}"></script>  
        <script type="text/javascript" src="{{asset('js/maps.js')}}"></script>


        @yield('skripte_dole')
</html>