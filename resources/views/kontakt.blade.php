@extends('layout')

@section('title','Kontakt - Trip Apartmani Beograd')


@section('glavni_sadrzaj')
    <!-- wrapper -->	
            <div id="wrapper">
                <!--content-->  
                <div class="content">
                    
                    <!--section -->  
                    <section id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Ako tražite udoban i povoljan smeštaj u centru Beograda, na pravom ste mestu! 
                                            

                                             </h3>
                                        </div>
                                        
                                        <p>
                                        Trip-Apartmani su porodična firma koja se bavi kratkoročnim izdavanjem apartmana u centru Beograda po sistemu „stan na dan“. Naš cilj je da vam ponudimo zavidan komfor i prvoklasnu uslugu po pristupačnoj ceni. Svi naši apartmani nalaze se u srcu Beograda, na pešačkoj udaljenosti od glavnih gradskih znamenitosti što ih čini savršenom bazom za upoznavanje grada. Rezervišite smeštaj bez provizije posredničkih agencija i ostvarite direktan kontakt sa osobama koje će brinuti o vašem smeštaju u Beogradu. 
                                        <br/><br/>Radujemo se viđenju s vama!
                                        </p>
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Iskreno vaši,</h3>
                                            <h3><span>TRIP-APARTMANI</span></h3>
                                        </div>
                                        <div class="list-author-widget-contacts">
                                            <ul>
                                                <li><span><i class="fa fa-phone"></i> Telefon:</span> <a href="#"> (+381) 061-194-77-22 </a></li>
                                                <li><span><i class="fa fa"></i>  </span> <a href="#"> (+381) 064-14-11-900</a></li>
                                                <li><span><i class="fa fa-envelope-o"></i> E-mail:</span> <a href="#">info@trip-apartmani.com</a></li>
                                            </ul>
                                        </div>
                                        <div style="text-align: left;">
                                            <ul>
                                                <li>
                                                <a href="https://www.instagram.com/tripapartmani/" target="_blank"><img style="height: 35px;" src="{{asset('images/insta.png')}}"></a>
                                                <a href="https://www.facebook.com/tripapartmani/?ref=br_rs" target="_blank"><img style="height: 35px;" src="{{asset('images/fb.png')}}"></a>
                                                <a href="https://twitter.com/ApartmaniTrip " target="_blank"><img style="height: 35px;" src="{{asset('images/twitter.png')}}"></a>
                                                <a href="whatsapp://send?abid=[users name]&text=[message text]" target="_blank"><img style="height: 35px;" src="{{asset('images/wa.png')}}"></a>
                                                <a href="viber://pa?chatURI=[public account URI]&text=[message text]" target="_blank"><img style="height: 35px;" src="{{asset('images/viber.png')}}"></a>
                                                </li>
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="list-single-main-wrapper fl-wrap">
                                        <div id="contact-form">
                                            <div id="message"></div>
                                            <form  class="custom-form" action="/kontaktiraj" method="post">
                                                {{csrf_field()}}
                                                <fieldset>
                                                    <label><i class="fa fa-user-o"></i></label>
                                                    <input required  type="text" name="ime" id="ime" placeholder="Ime i prezime *" value="" oninvalid="this.setCustomValidity('Molimo vas popunite ovo polje.')"
                                                           oninput="setCustomValidity('')">
                                                    <div class="clearfix"></div>
                                                    <label><i class="fa fa-envelope-o"></i>  </label>
                                                    <input required  type="text"  name="email" id="email" placeholder="Email *" value="" oninvalid="this.setCustomValidity('Molimo vas popunite ovo polje.')"
                                                           oninput="setCustomValidity('')">
													<div class="clearfix"></div>
													<label><i class="fa fa-phone"></i>  </label>
                                                    <input required  type="text"  name="telefon" id="telefon" placeholder="Telefon *" value="" oninvalid="this.setCustomValidity('Molimo vas popunite ovo polje.')"
                                                           oninput="setCustomValidity('')">
                                                    <textarea  placeholder="Poruka..."  name="poruka"  id="poruka" ></textarea>
                                                </fieldset>
                                                <button type="submit"  class="btn  big-btn  color-bg flat-btn" >Pošalji<i class="fa fa-angle-right"></i></button>
                                            </form>
                                        </div>
                                        <!-- contact form  end--> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    <div class="limit-box fl-wrap"></div>
                   
                </div>
                <!-- contentend -->
            </div>
            <!-- wrapper end -->


@stop