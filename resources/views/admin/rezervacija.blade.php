@extends('admin.layout')

@section('title')
    Rezervacija
@stop

@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">ADMIN - REZERVACIJA </h4>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                @if($id == -1)
                    <form onsubmit="return validirajDatume()" method="POST" action="/admin/sacuvaj_rezervaciju">
                @else
                    <form onsubmit="return validirajDatume()" method="POST" action="/admin/sacuvaj_izmenjenu_rezervaciju/{{$id}}">
                @endif
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="p-20">
                                    <div class="form-group m-b-20">
                                        <label for="reservation-name">Ime</label>
                                        <input name="reservation-name" type="text" class="form-control" id="reservation-name" required @if($id > 0) value="{{$rezervacija->ime}}" @endif >
                                    </div>

                                    <div class="form-group m-b-20">
                                        <label for="reservation-phone">Telefon</label>
                                        <input name="reservation-phone" type="text" class="form-control" id="reservation-phone" required @if($id > 0) value="{{$rezervacija->telefon}}" @endif >
                                    </div>

                                    <div class="form-group m-b-20">
                                        <label for="reservation-mail">Email</label>
                                        <input name="reservation-mail" type="text" class="form-control" id="reservation-mail" required @if($id > 0) value="{{$rezervacija->mail}}" @endif >
                                    </div>

                                    <div class="form-group m-b-20">
                                        <label for="reservation-note-client">Napomena klijent</label>
                                        <textarea name="reservation-note-client" class="form-control" id="reservation-note-client" rows="8">@if($id > 0) {{$rezervacija->napomena_klijent}} @endif </textarea>
                                    </div>

                                    <div class="form-group m-b-20">
                                        <label for="reservation-note-admin">Moja napomena</label>
                                        <textarea name="reservation-note-admin" class="form-control" id="reservation-note-admin" rows="8">@if($id > 0) {{$rezervacija->napomena_admin}} @endif </textarea>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4 m-b-10">
                                                <label for="reservation-days">Broj dana</label>
                                                <input name="reservation-days" type="number" step="1" min="1" class="form-control" id="reservation-days" required @if($id > 0) value="{{$rezervacija->br_dana}}" @endif>
                                            </div>
                                            <div class="col-sm-4 m-b-10">
                                                <label for="reservation-people">Broj osoba</label>
                                                <input name="reservation-people" type="number" min="1" step="1" class="form-control"  id="reservation-people" required @if($id > 0) value="{{$rezervacija->br_osoba}}" @endif>
                                            </div>
                                            <div class="col-sm-4 m-b-10">
                                                <label for="reservation-children">Broj dece</label>
                                                <input name="reservation-children"  type="number" min="0" step="1" class="form-control" id="reservation-children" required @if($id > 0) value="{{$rezervacija->br_dece}}" @endif>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                                <div class="p-20">

                                    <div class="form-group m-b-20">
                                        <label class="col-sm-2 control-label">Odaberite stan</label>
                                        <br/><br/>
                                        <div class="col-sm-12">
                                            <select name="reservation-select-flat" class="form-control" id="select-res">
                                                @foreach($stanovi as $stan)
                                                    <option @if($id > 0 and $rezervacija->id_stan == $stan->id) selected @endif name="stan-{{$stan->id}}" id="{{$stan->id}}" value="{{$stan->id}}">{{$stan->naziv}}, &nbsp;&nbsp;{{$stan->adresa}} &nbsp; ({{sizeof($rezervacije[$stan->id])}} rez.)</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br/><br/><br/><br/>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 m-b-10">
                                                <label for="reservation-date-start">Datum dolaska</label>
                                                <input name="reservation-date-start" type="date" class="form-control" id="reservation-date-start" required @if($id > 0) value="{{$rezervacija->dolazak}}" @endif>
                                            </div>
                                            <div class="col-sm-6 m-b-10">
                                                <label for="reservation-date-end">Datum odlaska</label>
                                                <input name="reservation-date-end" type="date" class="form-control" id="reservation-date-end" required @if($id > 0) value="{{$rezervacija->odlazak}}" @endif>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group m-b-20">
                                        <label for="reservation-price">Cena (eur)</label>
                                        <input name="reservation-price" type="number" min="0" step="any" class="form-control" id="reservation-price" required @if($id > 0) value="{{$rezervacija->cena}}" @endif >
                                    </div>

                                    <button  type="submit" class="btn btn-success save-event waves-effect waves-light">Sačuvaj rezervaciju</button>

                                </div>
                            </div>

                        </div>
                    </form>



            </div> <!-- container -->
            <br/><br/>
            <div class="m-t-20">
                <form class="col-xs-push-3 col-xs-6" onsubmit="return areYouSure()" method="POST" action="/admin/obrisi_rezervaciju/{{$id}}">
                    {{csrf_field()}}
                    <button  type="submit" class="btn btn-danger btn-block waves-effect waves-light">Obriši rezervaciju</button>
                </form>
                
            </div>
            <br/>
             <div class="m-t-20">
            <form class="col-xs-push-3 col-xs-6" method="get" action="/admin/rezervacije">
                    
                    <button  type="submit" class="btn btn-sucess btn-block waves-effect waves-light">Nazad na rezervacije</button>
            </form>
            </div>
            <br/><br/>

        </div> <!-- content -->

        <footer class="footer text-right">
            Trip-Apartmani ADMIN PANEL
        </footer>

    </div>



@stop


@section('afterScripts')
    <script src="{{asset('js/rezervacije.js')}}"></script>
@endsection