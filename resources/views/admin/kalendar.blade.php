@extends('admin.layout')

@section('title')
    Rezervacije
@stop

@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">ADMIN - REZERVACIJE </h4>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="col-md-6">
                    <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Odaberite stan</label>
                        <br/><br/>
                        <div class="col-sm-6">
                            <select class="form-control" id="select-res" onchange="selectedResChanged()">
                                @foreach($stanovi as $stan)
                                    <option name="stan-{{$stan->id}}" id="{{$stan->id}}" value="{{$stan->id}}">{{$stan->naziv}}, &nbsp;&nbsp;{{$stan->adresa}} &nbsp; ({{sizeof($rezervacije[$stan->id])}} rez.)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </form>
                </div>

                <br/><br/>

                <!-- BEGIN MODAL -->
                <div class="modal fade none-border" id="event-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Rezervacija</h4>
                            </div>
                            <div class="modal-body p-20"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Zatvori</button>
                                <button type="button" class="btn btn-success save-event waves-effect waves-light">Sačuvaj rezervaciju</button>
                                <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Obriši</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-9">
                    <div id="calendar"></div>
                </div> <!-- end col -->

                <br/><br/>

            </div> <!-- container -->
            <br/>
            &emsp;&emsp;<a href="/admin/rezervacija/-1" class="btn btn-success save-event waves-effect waves-light">Dodaj novu rezervaciju</a>
            <br/><br/>
        </div> <!-- content -->

        <footer class="footer text-right">
            Trip-Apartmani ADMIN PANEL
        </footer>

    </div>



@stop


@section('afterScripts')

    <!-- BEGIN PAGE SCRIPTS -->
    <script src="{{asset('plugins/moment/moment.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/js/fullcalendar.min.js')}}"></script>
    <script src="{{asset('assets/pages/jquery.fullcalendar.js')}}"></script>
    <script>storeReservations('<?php echo json_encode($rezervacije); ?>')</script>

@endsection