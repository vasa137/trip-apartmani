@extends('admin.layout')



@section('title')
    @if($id == -1)
        Unos stana
    @else
        {{$stan->naziv}}
    @endif
@stop


@section('content')
    <script src="{{asset('js/stan.js')}}"></script>

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            @if($id == -1)
                                <h4 class="page-title">Dodaj stan</h4>
                            @else
                                <h4 class="page-title">{{$stan->naziv}}</h4>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            @if($id == -1)
                                <h4 class="header-title m-t-0">Novi stan</h4>
                            @else
                                <h4 class="header-title m-t-0">Pregled/izmena stana</h4>
                            @endif
                            @if($id == -1)
                                <form method="POST" enctype="multipart/form-data" action="/admin/sacuvaj_stan">
                            @else
                                <form method="POST" enctype="multipart/form-data" action="/admin/sacuvaj_izmenjen_stan">
                            @endif
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="p-20">
                                                        <div class="form-group m-b-20">
                                                            <label for="property-name">Naziv stana</label>
                                                            <input name="property-name" type="text" class="form-control" id="property-name" required @if($id > 0) value="{{$stan->naziv}}" @endif >
                                                        </div>

                                                        <div class="form-group m-b-20">
                                                            <label for="property-desc">Opis</label>
                                                            <textarea name="property-desc" class="form-control" id="property-desc" rows="8" required>@if($id > 0) {{$stan->opis}} @endif </textarea>
                                                        </div>

                                                        <div class="form-group m-b-20">
                                                            <label for="property-desc-en">Opis na engleskom</label>
                                                            <textarea name="property-desc-en" class="form-control" id="property-desc-en" rows="8">@if($id > 0) {{$stan->opis_eng}} @endif </textarea>
                                                        </div>
                                                        <br/>
                                                        <h4>
                                                            Lokacija
                                                        </h4>
                                                        <br/><br/>
                                                        <div class="form-group m-b-20">
                                                            <label for="property-address">Adresa</label>
                                                            <input name="property-address" type="text" class="form-control" id="property-address"  required @if($id > 0) value="{{$stan->adresa}}" @endif>
                                                        </div>
                                                        <div class="form-group m-b-20">
                                                            <label for="property-location">Opis lokacije</label>
                                                            <input name="property-location" type="text" class="form-control" id="property-location" required @if($id > 0) value="{{$stan->lokacija}}" @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-6 m-b-10">
                                                                    <label for="property-N-coord">N koordinata</label>
                                                                    <input name="property-N-coord" type="number" min="-180" max="180" step="any" class="form-control"  id="property-N-coord" required @if($id > 0) value="{{$stan->x}}" @endif>
                                                                </div>
                                                                <div class="col-sm-6 m-b-10">
                                                                    <label for="property-E-coord">E koordinata</label>
                                                                    <input name="property-E-coord" type="number" min="-180" max="180" step="any" class="form-control"  id="property-E-coord" required @if($id > 0) value="{{$stan->y}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>
                                                        <h4>
                                                            Opšte informacije
                                                        </h4>
                                                        <br/><br/>

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-3 m-b-10">
                                                                    <label for="property-sqm">Kvadratura (m^2)</label>
                                                                    <input name="property-sqm" type="number" step="any" min="1" class="form-control" id="property-sqm" required @if($id > 0) value="{{$stan->kvadratura}}" @endif>
                                                                </div>
                                                                <div class="col-sm-2 m-b-10">
                                                                    <label for="property-rooms">Broj soba</label>
                                                                    <input name="property-rooms" type="number" min="1" step="0.5" class="form-control"  id="property-rooms" required @if($id > 0) value="{{$stan->sobe}}" @endif>
                                                                </div>
                                                                <div class="col-sm-3 m-b-10">
                                                                    <label for="property-max-people">Maksimalan broj ljudi</label>
                                                                    <input name="property-max-people" onchange="checkRadiosMaxPeople(this.value)" type="number" min="1" step="1" class="form-control" id="property-max-people" required @if($id > 0) value="{{$stan->kapacitet}}" @endif>
                                                                </div>
                                                                <div class="col-sm-2 m-b-10">
                                                                    <label for="property-beds">Broj kreveta</label>
                                                                    <input name="property-beds" type="text" class="form-control" id="property-beds"  @if($id > 0) value="{{$stan->kreveta}}" @endif>
                                                                </div>
                                                                <div class="col-sm-2 m-b-10">
                                                                    <label for="property-floor">Sprat</label>
                                                                    <input name="property-floor" type="text" class="form-control" id="property-floor" required @if($id > 0) value="{{$stan->sprat}}" @endif>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group m-b-20">
                                                            <label>Karakteristike</label>
                                                            <div class="row">

                                                                <div class="col-xs-6">
                                                                    <?php for($i = 0; $i < sizeof($funkcije)/2; $i++){
                                                                    $f = $funkcije[$i];
                                                                    ?>
                                                                    <div class="checkbox checkbox-primary m-b-15">
                                                                        <input id="checkbox-{{$f['id']}}" name="f[{{$f['id']}}]" type="checkbox" @if($id > 0 and isset($stan_funkcije[$f['id']])) checked @endif >
                                                                        <label for="checkbox-{{$f['id']}}">
                                                                            {{$f['naziv']}}
                                                                        </label>
                                                                    </div>

                                                                    <?php } ?>
                                                                </div>

                                                                <div class="col-xs-6">
                                                                    <?php for($i = sizeof($funkcije)/2; $i < sizeof($funkcije); $i++){
                                                                    $f = $funkcije[$i];
                                                                    ?>
                                                                    <div class="checkbox checkbox-primary m-b-15">
                                                                        <input id="checkbox-{{$f['id']}}" name="f[{{$f['id']}}]" type="checkbox" @if($id > 0 and isset($stan_funkcije[$f['id']])) checked @endif >
                                                                        <label for="checkbox-{{$f['id']}}">
                                                                            {{$f['naziv']}}
                                                                        </label>
                                                                    </div>

                                                                    <?php } ?>
                                                                </div> <!-- end col -->

                                                            </div> <!-- end row -->
                                                        </div>
                                                    </div>
                                                    <!-- end class p-20 -->
                                                    <div class="form-group m-b-20">
                                                        <label for="property-video">Video URL</label>
                                                        <input name="property-video" type="text" class="form-control" id="property-video"  maxlength="999" required @if($id > 0) value="{{$stan->video}}" @endif>
                                                    </div>
                                                </div> <!-- end col -->

                                                <div class="col-md-6">
                                                    <div class="p-20">


                                                        <div class="form-group m-b-20">
                                                            <label class="m-b-10">Tip cene</label>
                                                            <br/>
                                                            <div class="radio radio-info radio-inline">
                                                                <input onclick="typePriceRadioClicked(1)" type="radio" id="property-price-array" value="niz"
                                                                       name="property-price" @if($id == -1) checked @elseif($stan->tip_cene == 'niz') checked @endif>
                                                                <label for="property-price-array"> Prema broju noćenja </label>
                                                            </div>
                                                            <div class="radio radio-info radio-inline">
                                                                <input onclick="typePriceRadioClicked(2)" type="radio" id="property-price-table" value="tabela"
                                                                       name="property-price" @if($id > 0 and $stan->tip_cene == 'tabela') checked @endif>
                                                                <label for="property-price-table"> Prema broju noćenja i broju ljudi </label>
                                                            </div>

                                                            <div style="display: none" id="error-radio-price" class="error-message">
                                                                <br/>Morate definisati maksimalan broj ljudi sa leve strane
                                                            </div>
                                                        </div>
                                                        <br/>
                                                        <div id="price-arr">
                                                            <div id="price-arr-content" class="form-group">
                                                                @if($id == -1 or $stan->tip_cene == 'tabela')
                                                                    <div id="price-arr-row-1" class="row">
                                                                        <div class="col-sm-5 m-b-10">
                                                                            <label for="arr-price-nights-1">Broj noćenja</label>
                                                                            <input name="arr-price-nights[]" type="number" min="1" step="1" class="form-control" id="arr-price-nights-1" required>
                                                                        </div>
                                                                        <div class="col-sm-6 m-b-10">
                                                                            <label for="arr-price-val-1">Cena (eur)</label>
                                                                            <input name="arr-price-vals[]" type="number" min="1" step="any" class="form-control" id="arr-price-val-1" required>

                                                                        </div>
                                                                        <div class="col-sm-1 m-b-10">
                                                                            <br/>
                                                                            <button disabled id="arr-button-x-{{($i + 1)}}" class="btn btn-icon waves-effect waves-light btn-danger" onclick="removeArrRow()"> <i class="fa fa-remove"></i> </button>
                                                                        </div>
                                                                    </div>
                                                                @elseif($id > 0 and $stan->tip_cene == 'niz')
                                                                    <?php for($i = 0; $i < sizeof($noci); $i++){ ?>
                                                                    <div id="price-arr-row-{{($i + 1)}}" class="row">
                                                                        <div class="col-sm-5 m-b-10">
                                                                            <label for="arr-price-nights-{{($i + 1)}}">Broj noćenja</label>
                                                                            <input name="arr-price-nights[]" type="number" min="1" step="1" class="form-control" id="arr-price-nights-{{($i + 1)}}" required value="{{$noci[$i]}}">
                                                                        </div>
                                                                        <div class="col-sm-6 m-b-10">
                                                                            <label for="arr-price-val-{{($i + 1)}}">Cena (eur)</label>
                                                                            <input name="arr-price-vals[]" type="number" min="1" step="any" class="form-control" id="arr-price-val-{{($i + 1)}}" required value="{{$cene[$i]}}">

                                                                        </div>
                                                                        <div class="col-sm-1 m-b-10">
                                                                            <br/>
                                                                            <button @if($i == 0 or $i  + 1 < sizeof($noci))disabled @endif id="arr-button-x-{{($i + 1)}}" onclick="removeArrRow()" class="btn btn-icon waves-effect waves-light btn-danger"> <i class="fa fa-remove"></i> </button>
                                                                        </div>
                                                                    </div>
                                                                    <?php } ?>
                                                                @endif


                                                            </div>

                                                            <div class="row" style="display:flex; justify-content:center;">
                                                                <button onclick="addPriceArrayRow()" type="button" class="btn btn-teal btn-rounded waves-light waves-effect w-md">Dodaj broj noćenja</button>
                                                            </div>

                                                        </div>

                                                        <div style="display: none;" id="price-tab" >
                                                            <div id="price-tab-content">
                                                                @if($id > 0 and $stan->tip_cene == 'tabela')
                                                                    <?php for($i = 0; $i < sizeof($noci); $i++){ ?>
                                                                    <div class="form-group"  id="price-tab-row-{{($i + 1)}}">
                                                                        <div class="row">
                                                                            <div class="col-sm-5 m-b-10">
                                                                                <label for="price-nights-{{($i + 1)}}">Broj noćenja</label>
                                                                                <input name="tab-price-nights[]" type="number" min="1" step="1" class="form-control" id="price-nights-{{($i + 1)}}" required value="{{$noci[$i]}}">
                                                                            </div>
                                                                            <div class="col-sm-1 m-b-10">
                                                                                <br/>
                                                                                <button @if($i == 0 or $i + 1 < sizeof($noci)) disabled @endif id="tab-button-x-{{($i + 1)}}" class="btn btn-icon waves-effect waves-light btn-danger" onclick="removeTabRow()"> <i class="fa fa-remove"></i> </button>
                                                                            </div>
                                                                        </div>
                                                                        <br/>
                                                                        <div id="table-combinations-{{($i + 1)}}">
                                                                            <?php for($j = 0; $j < sizeof($cene[1]); $j++){ ?>
                                                                            <div class="row" id="table-comb-row-' + i + '-' + j + '">
                                                                                <div class="col-sm-6 m-b-10">

                                                                                    <label for="price-people-{{($i + 1)}}-{{($j + 1)}}">Broj ljudi</label>
                                                                                    <input readonly name="tab-price-people[{{($i + 1)}}][]" type="number" min="1" step="1" class="form-control" id="price-people-{{($i + 1)}}-{{($j + 1)}}" required value="{{$osobe[$i + 1][$j]}}">
                                                                                </div>
                                                                                <div class="col-sm-6 m-b-10">
                                                                                    <label for="price-nights-val-{{($i + 1)}}-{{($j + 1)}}">Cena (eur)</label>
                                                                                    <input name="tab-price-vals[{{($i + 1)}}][]" type="number" min="1" step="any" class="form-control" id="price-nights-val-{{($i + 1)}}-{{($j + 1)}}" required value="{{$cene[$i + 1][$j]}}">
                                                                                </div>
                                                                            </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <?php } ?>
                                                                        </div>
                                                                        @endif
                                                                    
                                                            </div>
                                                            <div class="row" style="display:flex; justify-content:center;">
                                                                <button onclick="addPriceTableRow()" type="button" class="btn btn-teal btn-rounded waves-light waves-effect w-md">Dodaj broj noćenja</button>
                                                            </div>
                                                        </div>

                                                        <br/><br/>

                                                        <div class="form-group m-b-20">
                                                            <div class="form-group">
                                                                <label class="control-label">Glavna slika</label>
                                                                <input onchange="previewImage(this)" name="main_image" id="main_image" type="file" class="filestyle" data-buttonname="btn-default">
                                                            </div>
                                                            <div class="form-group col-xs-12">

                                                                <img id="main_img_prev" @if($id > 0) src="{{asset('images\\stanovi\\' . $id .'\\glavna.jpg')}}"  @endif alt="image"
                                                                     class="img-responsive col-xs-push-4 col-xs-4"/>
                                                            </div>
                                                        </div>

                                                        <div class="p-20">
                                                            <div class="form-group clearfix">
                                                                <label class="control-label">Dodatne slike</label>
                                                                <div class="col-sm-12 padding-left-0 padding-right-0">
                                                                    <input type="file" name="files[]" id="filer_input1"
                                                                           multiple="multiple" accept="image/*">
                                                                </div>
                                                            </div>
                                                            @if($id > 0)
                                                                <div class="form-group">
                                                                    <div id="carousel-example-captions" data-ride="carousel" class="carousel slide">
                                                                        <ol class="carousel-indicators">
                                                                            <?php for($i = 0; $i < sizeof($slike); $i++){ ?>
                                                                            <li data-target="#carousel-example-captions" data-slide-to="0" @if($i == 0) class="active" @endif></li>
                                                                            <?php } ?>
                                                                        </ol>
                                                                        <div role="listbox" class="carousel-inner">
                                                                            <?php for($i = 0; $i < sizeof($slike); $i++){ ?>
                                                                            <div class="@if($i== 0) item active @else item @endif">
                                                                                <img  src="{{asset('images\\stanovi\\' . $id . '\\' . $slike[$i])}}" alt="First slide image">
                                                                                <div class="carousel-caption">
                                                                                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger" onclick="removeImg('{{$slike[$i]}}', 'img-{{$i}}')"> <i class="fa fa-remove"></i> </button>
                                                                                    <h3 style="color:white" id="img-{{$i}}"></h3>
                                                                                </div>
                                                                            </div>
                                                                            <?php }?>
                                                                        </div>
                                                                        <a href="#carousel-example-captions" role="button" data-slide="prev" class="left carousel-control"> <span aria-hidden="true" class="fa fa-angle-left"></span> <span class="sr-only">Previous</span> </a>
                                                                        <a href="#carousel-example-captions" role="button" data-slide="next" class="right carousel-control"> <span aria-hidden="true" class="fa fa-angle-right"></span> <span class="sr-only">Next</span> </a>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                        </div>



                                                        <!-- end class p-20 -->
                                                    </div> <!-- end col -->

                                                </div> <!-- end row -->

                                                <br/><br/>
                                                <div class="text-center clearfix">
                                                    @if($id == -1)
                                                        <button type="submit" class="btn btn-success waves-effect waves-light">Sačuvaj stan</button>
                                                    @else
                                                        <button type="submit" class="btn btn-success waves-effect waves-light" onclick="appendInputsImgToDel()">Sačuvaj stan</button>
                                                    @endif
                                                </div>
                                                <input type="hidden" name="id" value="{{$id}}"/>
                                                <div id="toDelDiv">

                                                </div>
                                            </div>



                                        </form>
                                      <!-- end form -->
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->
    </div>
    <br/><br/>
    <footer class="footer text-right">
        Trip-Apartmani ADMIN PANEL
    </footer>
@stop

@section('afterScripts')
    @if($id > 0)
        <script>setCounters('{{$stan->tip_cene}}', '{{sizeof($noci)}}')</script>
        @if($stan->tip_cene == 'tabela')
            <script>setLastMaxPeople('{{sizeof($osobe[1])}}')</script>
        @endif
    @endif
@stop