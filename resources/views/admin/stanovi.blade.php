@extends('admin.layout')

@section('title')
    Apartmani
@stop

@section('content')
<script src="{{asset('js/stan.js')}}"></script>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            
            <div class="row">
				
				<div class="page-title-box">
				    <div class="col-xs-8">
                    <h4 class="page-title">ADMIN - APARTMANI </h4>
                    </div>
                    <div class="col-xs-4">
                    <a href="http://www.trip-apartmani.com/admin/dodaj-stan" type="button" class="btn btn-success btn-block waves-effect waves-light">Dodaj novi apartman</a>
                    </div>
                    <div class="clearfix"></div>
                
				</div>
				
					
            
                    
				
			</div>
            <!-- end row -->


            <div class="row">

            	@foreach($stanovi as $s)
                <div class="col-md-4 col-sm-6">
                    <div class="property-card">
                        <div class="property-image">
                            <a href="/admin/stan/{{$s->id}}">
                            	<img style="height:280px;width:100%;" src="{{asset('images/stanovi')}}/{{$s->id}}/glavna.jpg" alt="">         
                             </a>
                        </div>

                        <div class="property-content">
                            <div class="listingInfo">
                                <div class="">
                                    <h5 class="text-success m-t-0">od {{$minCene[$s->id]}}€</h5>
                                </div>
                                <div class="">
                                    <h3 class="text-overflow"><a href="/admin/stan/{{$s->id}}" class="text-dark">{{$s->naziv}}</a></h3>
                                    <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>{{$s->adresa}}</p>

                                    <div class="row text-center">
                                        <div class="col-xs-3">
                                            <h4>{{$s->kvadratura}} m2</h4>
                                            <p class="text-overflow" title="Square Feet">Površina</p>
                                        </div>
                                        <div class="col-xs-3">
                                            <h4>{{$s->sobe}}</h4>
                                            <p class="text-overflow" title="Bedrooms">Broj soba</p>
                                        </div>
                                        <div class="col-xs-3">
                                            <h4>{{$s->kreveta}}</h4>
                                            <p class="text-overflow" title="Bedrooms">Kreveta</p>
                                        </div>
                                        <div class="col-xs-3">
                                            <h4>do {{$s->kapacitet}}</h4>
                                            <p class="text-overflow" title="Parking Space">Osoba</p>
                                        </div>
                                    </div>

                                    <div class="m-t-20">
                                        <a href="/admin/stan/{{$s->id}}" type="button" class="btn btn-success btn-block waves-effect waves-light">Detaljnije / Izmeni</a>
                                    </div>

                                    <div class="m-t-20">
                                        <form  onsubmit="return areYouSure()" method="POST" action="/admin/obrisi-stan/{{$s->id}}">
                                            {{csrf_field()}}
                                            <button  type="submit" class="btn btn-danger btn-block waves-effect waves-light">Obriši</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <!-- end. Card actions -->
                        </div>
                        <!-- /inner row -->
                    </div>
                    <!-- End property item -->
                </div>
                <!-- end col -->
                @endforeach
            </div>
            <!-- end row -->




        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer text-right">
       Trip-Apartmani ADMIN PANEL
    </footer>

</div>



@stop