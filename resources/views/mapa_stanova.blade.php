

@extends('layout')

@section('title','Mapa Stanova - Trip Apartmani Beograd')

@section('act_mapa_stanova')
class="act-link"
@stop


@section('glavni_sadrzaj')
<input id="stanovi" type="hidden" value="<?php echo htmlspecialchars(json_encode($stanovi)); ?>">
<!-- wrapper -->	
<div id="wrapper">
    <div class="content">
        <!-- Map -->
        <div class="map-container fw-map">
            <div id="map-main"></div>
            <ul class="mapnavigation">
                <li><a href="#" class="prevmap-nav">Prev</a></li>
                <li><a href="#" class="nextmap-nav">Next</a></li>
            </ul>                        
        </div>
        <!-- Map end -->   
        <!-- section-->  
        <section class="gray-bg no-pading no-top-padding">
            <div class="col-list-wrap  center-col-list-wrap left-list">
                <div class="container">
                    
                    <!-- list-main-wrap-->
                    <div class="list-main-wrap fl-wrap card-listing">
                        <!-- listing-item -->
                        @foreach($stanovi as $s)
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <a href="/stan/{{$s->naziv}}/{{$s->id}}"><img style="height:280px;width:100%;" src="images/stanovi/{{$s->id}}/glavna.jpg" alt="">
                                        
                                    <div class="overlay"></div>
                                   </a>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="/stan/{{$s->naziv}}/{{$s->id}}">od {{$minCene[$s->id]}} €</a>
                                    
                                    <h3><a href="/stan/{{$s->naziv}}/{{$s->id}}">{{$s->naziv}}</a></h3>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="geodir-category-location">
                                            <div class="row">
                                               <div style="float: left;" class="col-md-6" > 
                                                <h4 align="left">Adresa:</h4>
                                                </div>
                                                <div style="float: left;;"class="col-md-6" > 
                                                <h4 align="left">{{$s->adresa}}</h4>
                                                </div> 
                                            </div>
                                            
                                 
                                            <div class="row">
                                                <div style="float: left;" class="col-md-6"> 
                                                    <h4 align="left">Površina:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->kvadratura}} m2</h4>
                                                </div>
                                            </div>
                              
                                            <div class="row">
                                                <div style="float: left;" class="col-md-6" > 
                                                     <h4 align="left">Broj soba:</h4>
                                                </div>

                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->sobe}}</h4>
                                                </div>
                                            </div>
                                       
                                   
                                            <div class="row">
                                                <div align="left" style="float: left;" class="col-md-6" >
                                                    <h4 align="left">Broj kreveta:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                     <h4 align="left">{{$s->kreveta}}</h4>
                                                </div>
                                            </div>
                                       

                                        
                                            <div class="row">
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">Broj osoba:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->kapacitet}}</h4>
                                                </div>
                                            </div>

                                            @if($imaParking[$s->id])
                                                <img style="position:absolute;right:0px;bottom:1px;" width="25" height="25" src="/images/parking-logo.png"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end--> 
                        @endforeach                         
                    </div>
                    <!-- list-main-wrap end-->                           
                </div>
            </div>
        </section>
        <!-- section end-->  

    </div>

    <!-- content end--> 
</div>
<!-- wrapper end -->   


@stop

