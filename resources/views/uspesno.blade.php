@extends('layout')

@section('title','Uspesna rezervacija')


@section('glavni_sadrzaj')
<!-- wrapper -->	
<div id="wrapper">
    <!--content -->  
    <div class="content">
          
        <section  id="sec2">
            <div class="container">
                <div class="section-title">
                    <h2> Uspešno ste poslali upit.</h2>
                    <div class="section-subtitle">Uspešno ste poslali upit.</div>
                    <span class="section-separator"></span>
                    <p>Kontaktiraćemo Vas u najkraćem mogućem roku kako bismo potvrdili Vašu rezervaciju.</p>
                    <p><strong>Hvala Vam na poverenju!</strong></p>
                </div>
                <div class="time-line-wrap fl-wrap">                            
                    <div class="timeline-end"><i class="fa fa-check"></i></div>
                </div>
                <div class="section-title">
                    <p><a href="/">Nazad na početnu stranu...</a></p>
                </div>
            </div>
        </section>
        <!-- section end -->
        <div class="limit-box"></div>
    </div>
    <!-- content end -->
</div>
<!-- wrapper end -->
@stop