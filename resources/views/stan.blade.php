@extends('layout')

@section('title',' - Trip Apartmani Beograd')


@section('act_stanovi')
class="act-link"
@stop

@section('skripte_dole')
   <script src="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js')}}"></script> 
   
   <script>
	window.onload= function(){
		document.getElementById('#datepicker1').value= "Datum dolaska";
		document.getElementById('datepicker2').value = "Datum odlaska";
	}
	</script>
@stop







@section('cenovnik')
 <!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">CENOVNIK</h4>
    </div>
    <!-- CENOVNIK POCETAK-->
	<div class="box-widget opening-hours">
	    
		<div class="box-widget-content">
			<ul>
                <table
                @if($stan->tip_cene == 'niz')
	             style="width:50%; margin:auto;
						                border: 1px solid black;
                                        border-collapse: collapse;"
                @else
                        style="  width:100%;
						                border: 1px solid black;
                                        border-collapse: collapse;"
                @endif>
						<tr>
                            <th style="text-align: center;">Broj noćenja</th>

                            @if($stan->tip_cene == 'niz')
                                <th align="right">
                                    <span  class="opening-hours-time">Cena</span>
                                </th>
                            @else
                                <?php for($j = 0; $j <sizeof($cene[1]); $j++){ ?>
                                <th align="right">
                                    <span  class="opening-hours-time">{{$j+1}} os.</span>
                                </th>
                                <?php } ?>
    						@endif
                         </tr>
						
						
						
						
						
					<?php for($i  = 0; $i < sizeof($noci); $i++){ ?>
						<tr >
						    <td style="text-align:center;">
						       <span class="opening-hours-day">{{$i + 1}}</span> 
						    </td>

                            @if($stan->tip_cene == 'niz')
                                <td align="right">
                                    <span class="opening-hours-time">{{$cene[$i]}} €</span>
                                </td>
                            @else
                                <?php for($j = 0; $j < sizeof($cene[1]); $j++){ ?>
                                    <td align="right">
                                        <span class="opening-hours-time">{{$cene[$i + 1][$j]}}€</span>
                                    </td>

                                <?php } ?>
                            @endif
						</tr>
						
					<?php } ?>
					
					</table>
			</ul>
            <br/>
            <p>Za više od 10 noćenja kontaktirajte nas putem emaila ili telefonom.</p>
		</div>
	</div>
	<!-- CENOVNIK KRAJ-->
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">ZATVORI</button>
    </div>
  </div>
  
</div>
</div>

@stop


@section('glavni_sadrzaj')
<div id="wrapper">
    <<input id="stanovi" type="hidden" value="<?php echo htmlspecialchars(json_encode($stan)); ?>">
    <!--  content  --> 
    <div class="content">
        <!--  section  --> 
        
        <!--  section end --> 
		
		
		
        <div class="scroll-nav-wrapper fl-wrap">
            <div class="container">
                <nav class="scroll-nav scroll-init">
                    <ul>
                        <!--<li><a href="#sec1">Top</a></li>-->
                        <li><a href="#sec2" style="font-size:16px;">Fotografije</a></li>
                        <li><a href="#sec3" style="font-size:16px;">Informacije</a></li>
                        
                    </ul>
                </nav>
                
            </div>
        </div>
        <!--  section  --> 
        <section class="gray-section no-top-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
						
						<div class="time-line-box" style="width:100%;">
                            <img src="/images/stanovi/{{$stan->id}}/glavna.jpg" alt="">
                        </div>
                        <div class="list-single-main-wrapper fl-wrap" id="sec2">
                            
                             <div class="list-single-main-item fl-wrap" id="sec1">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Fotografije</h3>
                                </div>
                                <!-- gallery-items   -->
                                <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                    <!-- 1 -->
                                    @foreach($slike as $slika)
                                    <div class="gallery-item">
                                        <div class="grid-item-holder">
                                            <div class="box-item">
                                                <img  src="\images\stanovi\{{$stan->id}}\{{$slika}}"   alt="{{$slika}}">
                                                <a href="\images\stanovi\{{$stan->id}}\{{$slika}}" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                   @endforeach
                                </div>
                                <!-- end gallery items -->                                 
                            </div>
                            <div class="list-single-main-item fl-wrap" id="sec3">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Opis </h3>
                                </div>
                                <p>{{$stan->opis}}</p>
                                
                                <span class="fw-separator"></span>
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Sadržaji</h3>
                                </div>
                                <div class="listing-features fl-wrap">
                                    <ul>
                                        @foreach($funkcije as $f)
                                            <li style="font-size:16px;"><i class="fa fa-check"></i> {{$f->naziv}}</li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>

                            <div class="box-widget-item fl-wrap">
                                <div style="padding-left: 30px;" class="box-widget-item-header">
                                    <h3>Rezervišite</h3>
                                </div>
                                <div class="box-widget opening-hours">
                                    <div class="box-widget-content">
                                        
                                        <form  method="post" action="/rezervisi" class="add-comment custom-form">
                                            {{csrf_field()}}
                                            <fieldset>
                                                <label><i class="fa fa-user-o"></i></label>
                                                <input type="text" name="ime" placeholder="Ime i prezime *" value="" required oninvalid="this.setCustomValidity('Molimo Vas popunite ovo polje.')"
                                                       oninput="setCustomValidity('')"/>
												<div class="clearfix"></div>

                                                <div class="clearfix"></div>
                                                <label><i class="fa fa-envelope-o"></i>  </label>
                                                <input type="text" name="email" placeholder="Email *" value="" required oninvalid="this.setCustomValidity('Molimo Vas popunite ovo polje.')"
                                                       oninput="setCustomValidity('')"/>
                                                <label><i class="fa fa-phone"></i>  </label>
                                                <input type="text" name="telefon" placeholder="Telefon *" value="" required oninvalid="this.setCustomValidity('Molimo Vas popunite ovo polje.')"
                                                       oninput="setCustomValidity('')"/>
                                                <div class="quantity fl-wrap">

                                                    <span style="padding-left:0px !important; color: #334e6f;text-align: left;font-size: 18px; font-weight: 600;">Broj osoba</span>
                                                    <div class="quantity-item">
                                                        <input type="button" value="-" class="minus">
                                                        <input type="text"   name="osobe" value="1" title="Qty" class="qty" size="4" required oninvalid="this.setCustomValidity('Molimo Vas popunite ovo polje.')"
                                                               oninput="setCustomValidity('')">
                                                        <input type="button" value="+" class="plus">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label><i class="fa fa-calendar-check-o"></i>  </label>
                                                        <input style="font-weight:500;" data-disabled-days="{{$crveni_dani}}" type="text" placeholder="Date" class="datepicker" name="datum_od" id="#datepicker1" data-large-mode="true" data-large-default="true" value="" required oninvalid="this.setCustomValidity('Molimo Vas popunite ovo polje.')"
                                                               oninput="setCustomValidity('')"/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label><i class="fa fa-calendar-check-o"></i>  </label>
                                                        <input style="font-weight:500;" data-disabled-days="{{$crveni_dani}}" type="text" placeholder="Date" class="datepicker" name="datum_do"  id="datepicker2"  data-large-mode="true" data-large-default="true" value="" required oninvalid="this.setCustomValidity('Molimo Vas popunite ovo polje.')"
                                                               oninput="setCustomValidity('')"/>
                                                    </div>
                                                </div>
                                                <textarea cols="40" rows="3" name="napomena" placeholder="Napomena :"></textarea>
                                            </fieldset>
                                            <button type="submit" class="btn  big-btn color-bg flat-btn">Pošalji upit </button>
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!--box-widget-wrap -->
                    <div class="col-md-4">
                        <div class="box-widget-wrap">
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap">
                               
                                <div class="box-widget">
                                    
                                    <div class="box-widget-content">
                                        <div class="list-author-widget-contacts list-item-widget-contacts">
                                            <ul>
                                                <li><span><i style="width:20px;" class="fa fa-map-marker"></i> Adresa:</span> <p style="text-align: right;">{{$stan->adresa}}</p></li>
												<li><span><i style="width:20px;" class="fa fa-home"></i> Površina:</span> <p style="text-align: right;">{{$stan->kvadratura}}</p></li>
												<li><span><i style="width:20px;" class="fa fa-window-maximize"></i> Spavaćih soba:</span> <p style="text-align: right;">{{$stan->sobe}}</p></li>
                                                <li><span><i style="width:20px;" class="fa fa-bed"></i> Kreveta:</span> <p style="text-align: right;">{{$stan->kreveta}}</p></li>
                                                <li><span><i style="width:20px;" class="fa fa-male"></i> Osoba:</span> <p style="text-align: right;">{{$stan->kapacitet}}</p></li>
												<li><button type="button" class="btn  big-btn color-bg flat-btn" data-toggle="modal" data-target="#myModal">CENOVNIK</button></li>
                                                
                                                
                                            </ul>
                                        </div>
                                        <div class="map-container">
                                            
                                            <div id="singleMap" data-latitude="{{$stan->x}}" data-longitude="{{$stan->y}}" data-mapTitle="Out Location"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->   
                            <!--box-widget-item -->
							<!--
                            <div class="box-widget-item fl-wrap">
                                <div class="box-widget-item-header">
                                    <h3>Cenovnik : </h3>
                                </div>
                                <div class="box-widget opening-hours">
                                    <div class="box-widget-content">
                                        <ul>
                                            @if($stan->tip_cene == 'niz')
                                                <li><span class="opening-hours-day">Broj noćenja </span><span class="opening-hours-time">Cena</span></li>
                                                <?php for($i  = 0; $i < sizeof($noci); $i++){ ?>
                                                    <li><span class="opening-hours-day">{{$noci[$i]}} </span><span class="opening-hours-time">{{$cene[$i]}} €</span></li>
                                                <?php } ?>
                                            @else
                                                <?php for($i  = 0; $i < sizeof($noci); $i++){ ?>
                                                    <hr>
                                                    <h5>Broj noćenja : {{$noci[$i]}}</h5>
                                                    <hr>
                                                    <br/>
                                                    <?php for($j = 0; $j < sizeof($cene[1]); $j++){ ?>
                                                        <li><span class="opening-hours-day">{{$osobe[$i + 1][$j]}} os. </span><span class="opening-hours-time">{{$cene[$i + 1][$j]}} €</span></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
							-->
                            <!--box-widget-item end -->  
                            <!--box-widget-item -->

                            <!--box-widget-item end -->                                          </div>
                    </div>
                    <!--box-widget-wrap end -->
                </div>
            </div>
        </section>
        <!--section -->
        <section class="gradient-bg">
            <div class="cirle-bg">
                <div class="bg" data-bg="images/bg/circle.png"></div>
            </div>
            <div class="container">
                <div class="join-wrap fl-wrap">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Kontaktirajte nas</h3>
                            <p style="font-size:16px; ">Za sve dodatne informacije možete nas kontaktirati putem telefona ili e-maila.</p>
                        </div>
                        <div class="col-md-4"><a href="/kontakt"  class="join-wrap-btn" style="color:rgb(77, 183, 254); font-size:14px;">Kontakt <i style="font-size:14px;" class="fa fa-envelope-o"></i></a></div>
                    </div>
                </div>
            </div>
        </section>
        <!--  section end --> 
        <div class="limit-box fl-wrap"></div>
        
    </div>
    <!--  content end  --> 
    <style>
        .modal {
   position: absolute;
   top: 10px;
   right: 100px;
   bottom: 0;
   left: 0;
   z-index: 10040;
   overflow: auto;
   overflow-y: auto;
}
    </style>
    
                  
                  
                
</div>

@stop