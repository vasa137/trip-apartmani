@extends('layout')

@section('title','Trip Apartmani Beograd')

@section('act_naslovna')
class="act-link"
@stop





@section('glavni_sadrzaj')
<div id="wrapper">
    <!-- define the calendar element -->
        
    <!-- Content-->   
    <div class="content">
        <!--section -->
        <section class="scroll-con-sec hero-section" data-scrollax-parent="true" id="sec1">
            <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
                <!-- slideshow-item --> 
                <div class="slideshow-item">
                    <div class="bg"  data-bg="images/beograd-stan-na-dan2.jpg"></div>
                </div>
                <!--  slideshow-item end  -->                           
            </div>
            <div class="overlay"></div>
            <div class="hero-section-wrap fl-wrap">
                <div class="container">
                    <div class="intro-item fl-wrap">
                        <h2>STAN NA DAN – BEOGRAD </h2>
                        <h3>Naši apartmani nalaze se na najboljim lokacijama u centru Beograda</h3>
                    </div>
                    
                    <div class="main-search-input-wrap">
                    <form method="GET" action="/stanovi">
                        
                        <div class="main-search-input fl-wrap">
                          
                            <div class="main-search-input-item">

                             <span class="fa fa-calendar col-md-1" style="display:inline"></span>
                              <input id="#datepicker1" type="date" name='dolazak' class="datepicker col-md-10" placeholder="Datum Dolaska"  data-large-mode="true" data-large-default="true"  />

                            </div>
                             <div class="main-search-input-item">
                                 <span class="fa fa-calendar col-md-1" style="display:inline"></span>
                                <input id="datepicker2" type="date" name='odlazak' class="datepicker col-md-10" placeholder="Datum Odlaska"  data-large-mode="true" data-large-default="true" />
                            </div>

                            <div class="main-search-input-item">
                                <span class="fa fa-male col-md-1" style="display:inline"></span>
                               <input type="number" class="col-md-10" name='br_osoba' placeholder="Broj osoba"/>
                            </div>

                            <button type="submit" class="main-search-button" style="font-weight: bold;">Pretraga</button>

                        </div>


                        <input name="parking" id="parking-checkbox" type="checkbox" style="margin-top:20px; margin-bottom:20px; -ms-transform: scale(1.3); /* IE */ -moz-transform: scale(1.3); /* FF */ -webkit-transform: scale(1.3); /* Safari and Chrome */ -o-transform: scale(1.3); /* Opera */"/>
                        <label for="parking-checkbox" style="color:white;font-size:21px; padding:8px; font-weight: 500;">Parking</label>

                    </form>
                    </div>
                </div>
            </div>
            <div class="bubble-bg"> </div>
            <div class="header-sec-link">
                <div class="container"><a href="#sec2" class="custom-scroll-link">Pogledajte ponudu</a></div>
            </div>
        </section>

        <!--section -->
        <section class="gray-section" id="sec2">
            <div class="container">
                <div class="section-title">
                    <h2>Naši apartmani</h2>
                    <div class="section-subtitle">Pogledajte sve naše apartmane...</div>
                    <span class="section-separator"></span>
                    <p>Pogledajte sve apartmane iz naše ponude i odaberite idealno rešenje za vas.</p>
                </div>
            </div>
            <!-- carousel --> 
            <div class="list-carousel fl-wrap card-listing ">
                <!--listing-carousel-->
                <div class="listing-carousel  fl-wrap ">
                    @foreach($stanovi as $s)
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img" >
                                    <a href="/stan/{{$s->naziv}}-stan-na-dan-beograd/{{$s->id}}">

                                        <div class="list-carousel fl-wrap card-listing " style="padding:0px;">
                                            <!--listing-carousel-->
                                            <div class="client-carousel  fl-wrap "  >
                                            <div class="slick-slide-item">
                                                <!-- listing-item -->
                                                <div class="listing-item">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img" style="display:flex;justify-content:center;">
                                                            <img  src="/images/stanovi/{{$s->id}}/glavna.jpg"  style="max-height: 200px; /*width:auto;*/"  alt="">
                                                            <div class="overlay"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end-->
                                            </div>
                                            @foreach($slike[$s->id] as $slika)
                                                <!--slick-slide-item-->
                                                <div class="slick-slide-item">
                                                    <!-- listing-item -->
                                                    <div class="listing-item">
                                                        <article class="geodir-category-listing fl-wrap">
                                                            <div class="geodir-category-img" style="display:flex;justify-content:center;">
                                                                <img  src="/images/stanovi/{{$s->id}}/{{$slika}}"  style="max-height: 200px; /*width:auto;*/" alt="{{$slika}}">
                                                                <div class="overlay"></div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                    <!-- listing-item end-->
                                                </div>
                                            @endforeach

                                            </div>
                                            <!--listing-carousel end-->
                                            <div class="swiper-button-prev2 sw-btn"><i class="fa fa-chevron-left"></i></div>
                                            <div class="swiper-button-next2 sw-btn"><i class="fa fa-chevron-right"></i></div>
                                        </div>
                                   <!-- <img src="images/stanovi/{{$s->id}}/glavna.jpg" style="height: 200px; " alt=""> -->
                                    <div class="overlay"></div>
                                    </a>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="/stan/{{$s->naziv}}-stan-na-dan-beograd/{{$s->id}}">od {{$cene[$s->id]}} €</a>

                                    <h3><a href="/stan/{{$s->naziv}}-stan-na-dan-beograd/{{$s->id}}">{{$s->naziv}}</a></h3>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="geodir-category-location">

                                            <div class="row listaRow">
                                               <div style="float: left;" class="col-md-6" >
                                                <h4 align="left"  class="h4Lista"><i class="fa fa-map-marker iLista"></i>&nbsp;&nbsp;  Adresa:</h4>
                                                </div>
                                                <div style="float: left;;" class="col-md-6" >
                                                <h4 align="left">{{$s->adresa}}</h4>
                                                </div>
                                            </div>

                                            <div class="row listaRow">
                                                <div style="float: left;" class="col-md-6">
                                                    <h4 align="left" class="h4Lista"><i class="fa fa-home iLista"></i>&nbsp;&nbsp; Površina:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left" style="display:flex; align-items: center;">{{$s->kvadratura}} m2</h4>
                                                </div>
                                            </div>

                                            <div class="row listaRow">
                                                <div style="float: left;" class="col-md-6" >
                                                     <h4 align="left" class="h4Lista" ><i  class="fa fa-window-maximize iLista"></i>&nbsp;&nbsp; Broj soba:</h4>
                                                </div>

                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->sobe}}</h4>
                                                </div>
                                            </div>


                                            <div class="row listaRow">
                                                <div align="left" style="float: left;" class="col-md-6" >
                                                    <h4 align="left" class="h4Lista"><i class="fa fa-bed iLista"></i>&nbsp;&nbsp; Broj kreveta:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                     <h4 align="left">{{$s->kreveta}}</h4>
                                                </div>
                                            </div>



                                            <div class="row listaRow">
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left" class="h4Lista" ><i  class="fa fa-male iLista"></i>&nbsp;&nbsp; Broj osoba:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->kapacitet}}</h4>
                                                </div>
                                            </div>
                                            @if($imaParking[$s->id])
                                                <img style="position:absolute;right:0px;bottom:1px;" width="40" height="40" src="/images/parking-logo.png"/>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    @endforeach
                </div>
                <!--listing-carousel end-->
                 <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
            </div>
            <!--  carousel end--> 
        </section>
        <!-- section end -->
        <!--section -->
        <section class="color-bg">
            <div class="shapes-bg-big"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="images-collage fl-wrap">
                            <div class="images-collage-title">Trip-<span>Apartmani </span></div>
                            <div class="images-collage-main images-collage-item"><img src="images/beograd-stan-na-dan-krugovi/hram-svetog-save.jpg" alt=""></div>
                            <div class="images-collage-other images-collage-item" data-position-left="23" data-position-top="10" data-zindex="2"><img src="images/beograd-stan-na-dan-krugovi/beogradska-stark-arena-beograd.jpg" alt=""></div>
                            <div class="images-collage-other images-collage-item" data-position-left="62" data-position-top="54" data-zindex="5"><img src="images/beograd-stan-na-dan-krugovi/kalemegdan-beograd.jpg" alt=""></div>
                            <div class="images-collage-other images-collage-item anim-col" data-position-left="18" data-position-top="70" data-zindex="11"><img src="images/beograd-stan-na-dan-krugovi/narodni-muzej-beograd.jpg" alt=""></div>
                            <div class="images-collage-other images-collage-item" data-position-left="37" data-position-top="90" data-zindex="1"><img src="images/beograd-stan-na-dan-krugovi/skadarlija-beograd.jpg" alt=""></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="color-bg-text section-title">
                            <h3 style="text-transform: none !important;">Višegodišnja tradicija i iskustvo</h3>
                            <p style="text-align:left; text-transform: none !important;">Ako tražite udoban i povoljan smeštaj u centru Beograda, na pravom ste mestu! Odaberite apartman na prestižnim lokacijama u centru Beograda. </p>
                            <a href="/kontakt" class="color-bg-link" style="color:rgb(77, 183, 254); font-size:14px;">O nama</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--section   end -->  
        <!--section -->  
        <section>
            <div class="container">
                <div class="section-title">
                    <h2>Kako rezervisati?</h2>
                    <div class="section-subtitle">Jednostavna rezervacija... </div>
                    <span class="section-separator"></span>
                    <p>Odaberite željeni apartman i izvršite rezervaciju u nekoliko jednostavnih koraka.</p>
                </div>
                <!--process-wrap  -->
                <div class="process-wrap fl-wrap">
                    <ul>
                        <li>
                            <div class="process-item">
                                <span class="process-count">01 . </span>
                                <div class="time-line-icon"><i class="fa fa-search"></i></div>
                                <h4> Izaberite apartman</h4>
                                <p>Jednostavnom pretragom izaberite aparman i datume dolaska i odlaska.<br></p>
                            </div>
                            <span class="pr-dec"></span>
                        </li>
                        <li>
                            <div class="process-item">
                                <span class="process-count">02 .</span>
                                <div class="time-line-icon"><i class="fa fa-envelope"></i></div>
                                <h4> Pošaljite nam upit</h4>
                                <p>Pošaljite nam upit sa potrebinm podacima.<br><br></p>
                            </div>
                            <span class="pr-dec"></span>
                        </li>
                        <li>
                            <div class="process-item">
                                <span class="process-count">03 .</span>
                                <div class="time-line-icon"><i class="fa fa-thumbs-up"></i></div>
                                <h4> Sačekajte potvrdu rezervacije</h4>
                                <p>U najkraćem mogućem roku kontaktiraćemo vas radi potvrde rezervacije.</p>
                            </div>
                        </li>
                    </ul>
                    <div class="process-end"><i class="fa fa-check"></i></div>
                </div>
                <!--process-wrap   end-->
            </div>
        </section>
        <section class="parallax-section" data-scrollax-parent="true">
            <div class="bg"  data-bg="images/beograd-stan-na-dan-krugovi/beograd-dunav-sava-stan-na-dan.jpg" data-scrollax="properties: { translateY: '100px' }"></div>
            <div class="overlay co lor-overlay"></div>
            <!--container-->
            <div class="container">
                <div class="intro-item fl-wrap">
                    <h2>Stan Na Dan Beograd</h2>
                    <h3>Apartmani se nalaze isključivo u centru Beograda!</h3>
                    <a class="trs-btn btn  big-btn circle-btn  dec-btn  flat-btn" href="/stanovi">Rezervišite </a>
                </div>
            </div>
        </section>
        <!-- section end -->        
        <!--section -->
        <section class="gray-section">
            <div class="container">
                <div class="section-title">
                    <h2>Trip Apartmani</h2>
                    <div class="section-subtitle">Trip Apartmani</div>
                    <span class="section-separator"></span>
                    <p>Pozicija naših apartmana idealna je za...</p>
                </div>
            </div>
            <!-- carousel -->
            <div class="list-carousel fl-wrap card-listing ">
                <!--listing-carousel-->
                <div class="listing-carousel  fl-wrap ">
                    
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="images/stan-na-dan/stan-na-dan-knez-mihailova.jpg" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    
                                    <h3><div class="event-boja-naslov">Upoznavanje grada</div></h3>
                                    <p>Trip-Apartmani nalaze se na pešačkoj udaljenosti od svih glavnih gradskih znamenitosti – Trga Republike, Kalemegdana, Skadarlije, Knez Mihailove ulice, Savamale, Hrama Svetog Save, Muzeja Nikole Tesle – pa vam za njihov obilazak i razgledanje neće biti neophodna vozila javnog saobraćaja, kao ni taksi prevoz.<br><br> </p>    
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="images/stan-na-dan/hala-pionir.jpg" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    
                                    <h3><div class="event-boja-naslov">Posetu sportskim dešavanjima</div></h3>
                                    <p>Svi naši apartmani smešteni su u samom srcu Beograda odakle linijama gradskog prevoza veoma brzo i jednostavno možete stići do svih značajnih sportskih objekata u gradu – Beogradske Arene, Stadiona Crvene zvezde i Partizana, kao i Hale Aleksandar Nikolić (nekadašnji Pionir). <br><br></p>    
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="images/stan-na-dan/delta-city-usce-beograd.jpg" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    
                                    <h3><div class="event-boja-naslov">Šoping</div></h3>
                                    <p>Pozicija apartmana iz naše ponude posetiocima Beograda nude mogućnost posete tržnim centrima i trgovačkim ulicama u kojima je moguće šopingovati do kasnih večernjih sati. Osetite atmosferu tržnog centra Ušće, Bulevara Kralja Aleksandra ili Knez Mihailove ulice i nabavite suvenir koji će vas podsećati na boravak u Srbiji i Beogradu. </p>    
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="images/stan-na-dan/beograd-splav-klub.jpg" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    
                                    <h3><div class="event-boja-naslov">Noćni život</div></h3>
                                    <p>Posetite neki od novobeogradskih splavova, brojnih klubova u Beton hali i Savamali, ili restorana u boemskoj Skadarliji i proverite zašto se Beograd često smatra Evropskom prestonicom noćnog života. Uz podignutu čašu i čuveno „živeli“ nazdravite lokalcima koji će vam svojom gostoprimljivošću izmamiti osmeh na lice. <br><br></p>    
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="images/stan-na-dan/narodni-muzej.jpg" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    
                                    <h3><div class="event-boja-naslov">Posetu kulturnih događaja</div></h3>
                                    <p>Beograd je najveći grad regiona, samim tim i mesto održavanja brojnih kulturnih manifestacija i festivala među kojima su najpopularniji Beer Fest, Belef, Bemus i Noć muzeja. Prepustite se čulnom uživanju i osetite mešavinu različitih kultura i tradicija kojima odiše Beogad.<br><br><br></p>    
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <!-- listing-item -->
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <img src="images/stan-na-dan/beogradski-sajam.jpg" alt="">
                                    <div class="overlay"></div>
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    
                                    <h3><div class="event-boja-naslov">Posetu sajamskim izložbama</div></h3>
                                    <p>Beogradski sajam mesto je susreta mnogih poslovnih ljudi iz regiona i mesto održavanja velikog broja značajnih međunarodnih sajamskih izložbi među kojima su najpopularniji Sajam turizma, Sajam automobila, Beogradski sajam knjiga i Sajam nameštaja.<br><br><br> </p>    
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end-->
                    </div>
                    <!--slick-slide-item end-->

                </div>
                <!--listing-carousel end-->
                <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
            </div>
            
            
            <!--  carousel end-->
        </section>
        <!-- section end -->

        <section class="gray-section">
            <div>
                <a href="/mapa-stanova" class="btn  big-btn circle-btn  dec-btn color-bg flat-btn"> izaberite lokaciju<i class="fa fa-eye"></i></a>
            </div>
        </section>
        

                <!--section -->
        <section class="color-bg">
            <div class="shapes-bg-big"></div>
            <div class="container">
                <div class=" single-facts fl-wrap">
                    <!-- inline-facts -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="250">250</div>
                                </div>
                            </div>
                            <h6>Zadovoljnih klijenata godišnje</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->
                    <!-- inline-facts  -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="843">843</div>
                                </div>
                            </div>
                            <h6>Najboljih ocena od strane klijenata</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->
                    <!-- inline-facts  -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="1311">1311</div>
                                </div>
                            </div>
                            <h6>Zadovoljnih klijenata do sada</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->                            
                    <!-- inline-facts  -->
                    <div class="inline-facts-wrap">
                        <div class="inline-facts">
                            <div class="milestone-counter">
                                <div class="stats animaper">
                                    <div class="num" data-content="0" data-num="345">345</div>
                                </div>
                            </div>
                            <h6>Preporuka</h6>
                        </div>
                    </div>
                    <!-- inline-facts end -->                             
                </div>
            </div>
        </section>
        <!-- section end -->
        <!--section -->
        <section>
            <div class="container">
                <div class="section-title">
                    <h2>Ocene i utisci</h2>
                    <div class="section-subtitle">Ocene i utisci</div>
                    <span class="section-separator"></span>
                    <p>Pogledajte samo neke od komentara naših klijenata...</p>
                </div>
            </div>
            <!-- testimonials-carousel --> 
            <div class="carousel fl-wrap">
                <!--testimonials-carousel-->
                <div class="testimonials-carousel single-carousel fl-wrap">
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Everything was perfect - the location, appartement facilities were amazing. Big plus is the parking spot which is available for visitors. Totally recommend. </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Alexandra</h4>
                            <span>Romania</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Stanovanje je popolnoma novo, čisto ter zelo okusno opremljeno. Iz balkona je mogoč pogled na Hram Sv. Save (na dvorišče). Prav tako je zagotovljeno parkirišče, kar je v BG zelo pomembno. Lastnik zelo prijazen, gostoljuben in dosegljiv kadarkoli za kakšne želje. 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Vesna</h4>
                            <span>Slovenia</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>The house looks great. Very clean. Hosts are really nice and try to do the best they can. Very recommended. 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Ross</h4>
                            <span>UK</span>
                        </div>
                    </div>
                    <!--slick-slide-item end--> 
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Very friendly host and the location is great. We had an amazing experience. 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Theodoros</h4>
                            <span>Cyprus</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->  
                     <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>L'appartement est encore mieux que sur les photos! Tout est neuf, propre et bien décoré. Il se situe dans une des rues les plus prisées de Belgrade, juste en face du musée Nikola Tesla et il y a une place de parking!! Que demander de plus! Je recommande à 200% et j'y reviendrais moi-même lors de mes prochains séjours à Belgrade. Un grand merci à Vladica pour sa gentillesse et ses conseils. 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Pamela</h4>
                            <span>France</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->  
                     <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>The apartment was brand new, very clean and comfortable. The host is very kind and responds immediately and also was very helpfull explaining us where to go and what to visit. Also the area is very peaceful and near the center. We totally recommend it. 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>ΕΥΑΓΓΕΛΟΣ</h4>
                            <span>Greece</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->  
                                         <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>•    Το σπιτι ηταν πολυ μοντερνο και Περιποιημένο! O οικοδεσποτης πολυ θερμος και αξιολογος ανθρωπος προθυμος για βοήθεια!! Η γειτονια του αγιου Σαββα Ιδιαίτερα πολιτισμενη με νοτες ιστοριας και ηθους! Γενικοτερα περασα απιθανα και θα το προτιμησω ξαναα το ιδιο σπιτι με τον ιδιο οικοδεσποτη! Ευχαριστώ και παλι για ολα! 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Αναστασιος Αμανατιδης</h4>
                            <span>Ελλάδα</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->  
                                         <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Veoma često dolazim u Beograd zbog posla. Nakon prvog boravka u Shining apartmanu prestao sam da tražim druge opcije za smeštaj.  Vladica je sjajan domaćin, uvek se lako dogovorimo oko svega. Takođe veoma je važno što aparman poseduje garažu, što je u centru grada bitno zbog ograničenog vremena u zonama za parkiranje. 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Aleksandar</h4>
                            <span>Kragujevac</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->  
                    <!--slick-slide-item-->
                    <div class="slick-slide-item">
                        <div class="testimonilas-text">
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <p>Prezadovoljna sam uslugom. Vladica je veoma ljubazan. Dočekao nas je iako smo stigli veoma kasno (oko 3h ujurtu). Stan je bio identičan kao na slikama, a higijena na visokom nivou. SVE POHVALE!!! :) 
                            </p>
                        </div>
                        <div class="testimonilas-avatar-item">
                            <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div>
                            <h4>Marijana</h4>
                            <span>Sarajevo</span>
                        </div>
                    </div>
                    <!--slick-slide-item end-->   
                </div>
                <!--testimonials-carousel end-->
                <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
            </div>
            <!-- carousel end-->
        </section>
        <!-- section end -->
        
    </div>
    
                    
    <!-- Content end -->      
</div> 


@stop


@section('skripte_dole')
<script>
window.onload= function(){
    document.getElementById('#datepicker1').value= "Datum dolaska";
    document.getElementById('datepicker2').value = "Datum odlaska";
}
</script>
@endsection