@extends('layout')

@section('title','Kako rezervisati?')

@section('act_kako')
class="act-link"
@stop


@section('glavni_sadrzaj')
<!-- wrapper -->	
<div id="wrapper">
    <!--content -->  
    <div class="content">
        <!--section -->  
        <section  id="sec2">
            <div class="container">
                <div class="section-title">
                    <h2>Kako rezervisati?</h2>
                    <div class="section-subtitle">Jednostavna rezervacija... </div>
                    <span class="section-separator"></span>
                    <p>Odaberite željeni apartman i izvršite rezervaciju u nekoliko jednostavnih koraka.</p>
                </div>
                <div class="time-line-wrap fl-wrap">
                    <!--  time-line-container  --> 
                    <div class="time-line-container">
                        <div class="step-item">Korak 1</div>
                        <div class="time-line-box tl-text tl-left">
                            <span class="process-count">01 . </span>
                            <div class="time-line-icon">
                                <i class="fa fa-search"></i>
                            </div>

                            <h4 class="koraci-title"> Izaberite apartman </h4>
                            <p>Jednostavnom pretragom izaberite aparman koji odgovara vašim potrebama.</p>
                        </div>
                        <div class="time-line-box tl-media tl-right">
                            <img src="images/stan-na-dan-bg1.jpg" alt="">
                        </div>
                    </div>
                    <!--  time-line-container --> 
                    <!--  time-line-container  --> 
                    <div class="time-line-container lf-im">
                        <div class="step-item">Korak 2</div>
                        <div class="time-line-box tl-text tl-right">
                            <span class="process-count">02 . </span>
                            <div class="time-line-icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <h4 class="koraci-title"> Pošaljite nam upit</h4>
                            <p>Pošaljite nam upit sa potrebinm podacima.</p>
                        </div>
                        <div class="time-line-box tl-media tl-left">
                            <img src="images/stan-na-dan-bg2.jpg" alt="">
                        </div>
                    </div>
                    <!--  time-line-container -->                             
                    <!--  time-line-container  --> 
                    <div class="time-line-container">
                        <div class="step-item">Korak 3</div>
                        <div class="time-line-box tl-text tl-left">
                            <span class="process-count">03 . </span>
                            <div class="time-line-icon">
                                <i class="fa fa-thumbs-up"></i>
                            </div>
                            <h4 class="koraci-title"> Sačekajte potvrdu rezervacije</h4>
                            <p>Kontaktiraćemo vas u najkraćem mogućem roku kako bismo potvrdili vašu rezervaciju. </p>
                        </div>
                        <div class="time-line-box tl-media tl-right">
                            <img src="images/stan-na-dan-bg3.jpg" alt="">
                        </div>
                    </div>
                    <!--  time-line-container -->                             
                    <div class="timeline-end"><i class="fa fa-check"></i></div>
                </div>
            </div>
        </section>
        <!-- section end -->
        <div class="limit-box"></div>
    </div>
    <!-- content end -->
</div>
<!-- wrapper end -->
@stop