@extends('layout')

@section('title','Stanovi - Trip Apartmani Beograd')

@section('act_stanovi')
class="act-link"
@stop


@section('glavni_sadrzaj')
<!-- wrapper -->	
<div id="wrapper">
    <div class="content">
       
        <!-- section-->  
        <section class="gray-bg no-pading no-top-padding">
            <div class="col-list-wrap  center-col-list-wrap left-list">
                <div class="container">
                    <div class="listsearch-maiwrap box-inside fl-wrap">
                        <div class="listsearch-header fl-wrap">
                            <h3>Pretraga @if($termin != 'Bilo koji termin'): <span>{{$termin}}</span>@endif </h3>
                            <div class="listing-view-layout">
                                <ul>
                                    <li><a class="grid active" href="#"><i class="fa fa-th-large"></i></a></li>
                                    <li><a class="list" href="#"><i class="fa fa-list-ul"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <!-- listsearch-input-wrap  -->
                        <div class="listsearch-input-wrap fl-wrap">

                                    <form method="GET" action="/stanovi">
                                        <div class="main-search-input fl-wrap" style="border:1px solid #4DB7FE;">

                                            <div class="main-search-input-item">

                                                <span class="fa fa-calendar col-md-1" style="display:inline"></span>
                                                <input id="#datepicker1" type="date" name='dolazak' class="datepicker col-md-10" placeholder="Datum Dolaska"  data-large-mode="true" data-large-default="true"  />

                                            </div>
                                            <div class="main-search-input-item">
                                                <span class="fa fa-calendar col-md-1" style="display:inline"></span>
                                                <input id="datepicker2" type="date" name='odlazak' class="datepicker col-md-10" placeholder="Datum Odlaska"  data-large-mode="true" data-large-default="true" />
                                            </div>

                                            <div class="main-search-input-item">
                                                <span class="fa fa-male col-md-1" style="display:inline"></span>
                                                <input type="number" class="col-md-10" name='br_osoba' placeholder="Broj osoba"/>
                                            </div>

                                            <button type="submit" class="main-search-button" style="font-weight: bold;">Pretraga</button>

                                        </div>


                                        <input name="parking" id="parking-checkbox" type="checkbox" style="margin-top:20px; margin-bottom:20px; -ms-transform: scale(1.3); /* IE */ -moz-transform: scale(1.3); /* FF */ -webkit-transform: scale(1.3); /* Safari and Chrome */ -o-transform: scale(1.3); /* Opera */"/>
                                        <label for="parking-checkbox" style="color:#4DB7FE;font-size:21px; padding:8px; font-weight: 500;">Parking</label>
                                    </form>


                        </div>
                        <!-- listsearch-input-wrap end -->   
                    </div>
                    <!-- list-main-wrap-->
                    <div class="list-main-wrap fl-wrap card-listing">
                        <!-- listing-item -->
                        @foreach($stanovi as $s)
                        <div class="listing-item">
                            <article class="geodir-category-listing fl-wrap">
                                <div class="geodir-category-img">
                                    <a href="/stan/{{$s->naziv}}/{{$s->id}}"><img style="height:280px;width:100%;" src="images/stanovi/{{$s->id}}/glavna.jpg" alt="">
                                        <div class="overlay"></div>
                                        </a>
                                    
                                   
                                </div>
                                <div class="geodir-category-content fl-wrap">
                                    <a class="listing-geodir-category" href="/stan/{{$s->naziv}}/{{$s->id}}">od {{$cene[$s->id]}} €</a>

                                    <h3><a href="/stan/{{$s->naziv}}/{{$s->id}}">{{$s->naziv}}</a></h3>
                                    <div class="geodir-category-options fl-wrap">
                                        <div class="geodir-category-location">
                                        	<div class="row listaRow" >
                                               <div style="float: left;" class="col-md-6" > 
                                                <h4 align="left" class="h4Lista"> <i class="fa fa-map-marker iLista"></i>&nbsp;&nbsp;&nbsp;Adresa:</h4>
                                                </div>
                                                <div style="float: left;;" class="col-md-6" >
                                                <h4 align="left">{{$s->adresa}}</h4>
                                                </div> 
                                            </div>
                                            
                                 
                                            <div class="row listaRow">
                                                <div style="float: left;" class="col-md-6"> 
                                                    <h4 align="left" class="h4Lista"><i class="fa fa-home iLista"></i>&nbsp;&nbsp;&nbsp;Površina:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->kvadratura}} m2</h4>
                                                </div>
                                            </div>
                              
                                            <div class="row listaRow">
                                                <div style="float: left;" class="col-md-6" > 
                                                     <h4 align="left" class="h4Lista"><i  class="fa fa-window-maximize iLista"></i>&nbsp;&nbsp;&nbspBroj soba:</h4>
                                                </div>

                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left" >{{$s->sobe}}</h4>
                                                </div>
                                            </div>
                                       
                                   
                                            <div class="row listaRow">
                                                <div align="left" style="float: left;" class="col-md-6" >
                                                    <h4 align="left" class="h4Lista"><i class="fa fa-bed iLista"></i>&nbsp;&nbsp;&nbsp;Broj kreveta:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                     <h4 align="left">{{$s->kreveta}}</h4>
                                                </div>
                                            </div>
                                       

                                        
                                            <div class="row listaRow">
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left" class="h4Lista"><i  class="fa fa-male iLista"></i>&nbsp;&nbsp;&nbspBroj osoba:</h4>
                                                </div>
                                                <div style="float: left;" class="col-md-6" >
                                                    <h4 align="left">{{$s->kapacitet}}</h4>
                                                </div>
                                            </div>
                                            @if($imaParking[$s->id])
                                                <img style="position:absolute;right:0px;bottom:1px;" width="40" height="40" src="/images/parking-logo.png"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- listing-item end--> 
                        @endforeach                         
                    </div>
                    <!-- list-main-wrap end-->                           
                </div>
            </div>
        </section>
        <!-- section end-->  

    </div>
    <!-- content end--> 
</div>
<!-- wrapper end -->   


@stop



@section('skripte_dole')
<script>
        window.onload = function () {

                document.getElementById('#datepicker1').value = "Datum dolaska";
                document.getElementById('datepicker2').value = "Datum odlaska";

        }

</script>
@endsection