<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@naslovna');


Route::get('/kontakt', function () {
    return view('kontakt');
});

Route::get('/uspesna-rezervacija', function () {
    return view('uspesno');
});

Route::get('/kontakt-uspesan', function () {
    return view('uspesno_kontakt');
});

Route::get('/kako-rezervisati', function () {
    return view('koraci');
});

Route::get('/pitanja', function () {
    return view('pitanja');
});

Route::get('/o-nama', function () {
    return view('o_nama');
});

Route::get('/mapa-stanova','StanController@mapa_stanova');

Route::get('/stanovi','StanController@stanovi');

Route::get('/stan/{link}/{id}','StanController@stan');

Route::get('/pretraga','StanController@pretraga');

Route::post('/kontaktiraj','MainController@kontaktiraj');

Route::post('/rezervisi','MainController@rezervacija');

//------------------ADMIN RUTE POCETAK---------------------------
/*
Route::group(['middleware' => ['auth', 'admin']], function()

{*/

Route::get('/admin','AdminController@stanovi');

Route::get('/admin/stanovi','AdminController@stanovi');

Route::get('/admin/stan/{id}','AdminController@stan');

Route::get('/admin/dodaj-stan','AdminController@dodaj_stan');

Route::get('/admin/rezervacije','AdminController@rezervacije');

Route::get('/admin/rezervacija/{id}','AdminController@rezervacija');

Route::post('/admin/plugins/jquery.filer/php/upload.php', 'AdminController@uploadImage');

Route::post('/admin/plugins/jquery.filer/php/remove_file.php', 'AdminController@removeImage');

Route::post('/admin/sacuvaj_stan', 'AdminController@sacuvajStan');

Route::post('/admin/sacuvaj_izmenjen_stan', 'AdminController@sacuvajIzmenjenStan');

Route::post('/admin/obrisi-stan/{id}', 'AdminController@brisiStan');

Route::post('/admin/sacuvaj_rezervaciju', 'AdminController@sacuvajRezervaciju');

Route::post('/admin/sacuvaj_izmenjenu_rezervaciju/{id}', 'AdminController@sacuvajIzmenjenuRezervaciju');

Route::post('/admin/obrisi_rezervaciju/{id}', 'AdminController@brisiRezervaciju');
//});
//------------------ADMIN RUTE KRAJ---------------------------


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/logout','MainController@odjavi_se');