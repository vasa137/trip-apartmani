<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Stan extends Model
{
   	protected $fillable = [
       'id', 'naziv','adresa','lokacija' ,'x', 'y','opis', 'opis_eng','kvadratura','sobe', 
       'kapacitet', 'kreveta', 'sprat','video', 'tip_cene',
    ];
    protected $table = 'stan';
    protected $table_name = 'stan';
    public $timestamps = false;
    public static function stan($id)
    {
    	return Stan::where('id',$id)->first();
    }

    private function popuniStan( $naziv,
                                 $opis,
                                 $opis_eng,
                                 $adresa,
                                 $lokacija,
                                 $x,
                                 $y,
                                 $kvadratura,
                                 $sobe,
                                 $kapacitet,
                                 $kreveta,
                                 $sprat,
                                 $video,
                                 $tip_cene){
        $stan = $this;

        $stan->naziv = $naziv;
        $stan->opis = $opis;
        $stan->opis_eng = $opis_eng;
        $stan->adresa = $adresa;
        $stan->lokacija = $lokacija;
        $stan->x = $x;
        $stan->y = $y;
        $stan->kvadratura =$kvadratura;
        $stan->sobe = $sobe;
        $stan->kapacitet = $kapacitet;
        $stan->kreveta = $kreveta;
        $stan->sprat=  $sprat;
        $stan->video = $video;
        $stan->tip_cene = $tip_cene;
    }

    public static function insert( $naziv,
                $opis,
                $opis_eng,
                $adresa,
                $lokacija,
                $x,
                $y,
                $kvadratura,
                $sobe,
                $kapacitet,
                $kreveta,
                $sprat,
               $video,
                $tip_cene){

        $stan = new Stan();

        $stan->popuniStan($naziv, $opis, $opis_eng, $adresa, $lokacija, $x, $y, $kvadratura, $sobe, $kapacitet, $kreveta, $sprat, $video, $tip_cene);
        $stan->save();

        return $stan->id;
    }

    public function azuriraj( $naziv,
                              $opis,
                              $opis_eng,
                              $adresa,
                              $lokacija,
                              $x,
                              $y,
                              $kvadratura,
                              $sobe,
                              $kapacitet,
                              $kreveta,
                              $sprat,
                              $video,
                              $tip_cene){

        $this->popuniStan($naziv, $opis, $opis_eng, $adresa, $lokacija, $x, $y, $kvadratura, $sobe, $kapacitet, $kreveta, $sprat, $video, $tip_cene);

        $this->save();
    }

    public static function brisi($idStana){
        Stan::where('id', $idStana)->delete();
    }

}
