<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Rezervacija extends Model
{
	protected $fillable = [
       'id', 'ime', 'id_stan', 	'br_dana', 	'br_osoba', 'br_dece', 	'mail', 'telefon', 	'cena' 	, 'napomena_klijent' ,'napomena_admin', 'dolazak' ,'odlazak',
    ];
    protected $table = 'rezervacija';
    protected $table_name = 'rezervacija'; 
    public $timestamps = false;


    public static function zauzetiStanovi($dolazak,$odlazak)
    {
    	return Rezervacija::whereBetween('dolazak', [$dolazak,$odlazak])->whereBetween('odlazak', [$dolazak,$odlazak])->get(['id_stan']);
    }

    public static function slobodniStanovi($dolazak,$odlazak, $br_osoba, $parking)
    {
        if($parking) {
            return DB::select("
            select s.*
            from stan s, stan_funkcija sf, funkcija f
            where $br_osoba  <= s.kapacitet
            and sf.id_stan = s.id
            and sf.id_funkcija = f.id
            and f.naziv = 'parking'
            and not exists(
              select *
              from rezervacija r 
              where r.id_stan = s.id
              and (('$dolazak' >= r.dolazak and '$dolazak' < r.odlazak) or ('$odlazak' >= r.dolazak and '$odlazak' < r.odlazak) or ('$dolazak' < r.dolazak and '$odlazak' >= r.odlazak))
            );
        ");
        } else{
            return DB::select("
            select s.*
            from stan s
            where $br_osoba  <= s.kapacitet
            and not exists(
              select *
              from rezervacija r 
              where r.id_stan = s.id
              and (('$dolazak' >= r.dolazak and '$dolazak' < r.odlazak) or ('$odlazak' >= r.dolazak and '$odlazak' < r.odlazak) or ('$dolazak' < r.dolazak and '$odlazak' >= r.odlazak))
            );
        ");
        }

    }
    public static function brisiPoId($id){
        Rezervacija::where('id', $id)->delete();
    }
    public static function brisi($idStana){
        Rezervacija::where('id_stan', $idStana)->delete();
    }

    public static function zaStan($idStana){
        return Rezervacija::where('id_stan', $idStana)->get();

    }

    public static function poId($id){
        return Rezervacija::where('id', $id)->first();
    }

    private function popuni($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena){
        $this->ime = $ime;
        $this->telefon = $telefon;
        $this->mail = $mail;
        $this->napomena_klijent = $napomena_klijent;
        $this->napomena_admin = $napomena_admin;
        $this->br_dana = $br_dana;
        $this->br_osoba = $br_osoba;
        $this->br_dece = $br_dece;
        $this->id_stan = $id_stan;
        $this->dolazak = $dolazak;
        $this->odlazak = $odlazak;
        $this->cena = $cena;
    }

    public static function insert($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena){
        $rez = new Rezervacija();

        $rez->popuni($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena);

        $rez->save();
    }

    public function azuriraj($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena){
        $this->popuni($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena);

        $this->save();
    }
   //
}
