<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class FunkcijaStan extends Model
{
	protected $fillable = [
        'id_stan','id_funkcija' 	
    ];
    protected $table = 'stan_funkcija';
    protected $table_name = 'stan_funkcija';
    public $timestamps = false;


    public static function sveZaStan($idStan)
    {
    	return FunkcijaStan::where('id_stan',$idStan)->get();
    }

    public static function insert($f, $sveFunkcije, $idStana){

        for($i = 0; $i < sizeof($sveFunkcije); $i++){
            $sf = $sveFunkcije[$i];
            if(isset($f[$sf['id']])){
                $funkcijaStan = new FunkcijaStan();
                $funkcijaStan->id_stan = $idStana;
                $funkcijaStan->id_funkcija = $sf['id'];

                $funkcijaStan->save();
            }
        }
    }


    public static function brisi($idStana){
        FunkcijaStan::where('id_stan', $idStana)->delete();
    }

    public static function imaParking($idStan){
        return DB::select("
            select * from
            stan_funkcija sf, funkcija f 
            where sf.id_funkcija = f.id
            and sf.id_stan = $idStan
            and f.naziv = 'parking'
        ");
    }
}
