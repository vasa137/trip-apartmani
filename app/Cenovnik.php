<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cenovnik extends Model
{
    protected $fillable = [
        'id_stan','cena', 'noci', 'osoba' 	
    ];
    protected $table = 'cenovnik';
    protected $table_name = 'cenovnik';
    public $timestamps = false;

    public static function minCena($stan)
    {
    	return Cenovnik::where('id_stan',$stan)->min('cena');
    }

     public static function maxCena($stan)
    {
        return Cenovnik::where('id_stan',$stan)->max('cena');
    }

     public static function cenaNiz($id,$i)
     {
     	return Cenovnik::where([['id_stan',$id],['noci',$i]])->first()->cena;
     }

     public static function cenaTabela($id,$i,$j)
     {
     	return Cenovnik::where([['id_stan',$id],['noci',$i],['osoba',$j]])->first();
     }

     public static function maxOsoba($idStan)
     {
     	return Cenovnik::where('id_stan',$idStan)->max('osoba');
     }

     public static function dohvatiCene($idStan){
         return Cenovnik::where('id_stan',$idStan)->orderBy('noci', 'ASC')->orderBy('osoba','ASC')->get();
     }

     public static function insert($idStana, $noc, $cena, $osoba){
        $c = new Cenovnik();
        $c->id_stan = $idStana;
        $c->cena = $cena;
        $c->noci = $noc;
        $c->osoba = $osoba;

        $c->save();

        return $c->id;
     }

     public static function brisi($idStana){
        Cenovnik::where('id_stan', $idStana)->delete();
     }
}
