<?php

namespace App\Http\Controllers;

use App\Stan;
use App\Cenovnik;
use App\Funkcije;
use App\FunkcijaStan;
use App\Rezervacija;
use Illuminate\Http\Request;
use File;
class AdminController extends Controller
{
   public function naslovna()
   {
   		return view('admin.home');
   }
   

   public function stanovi()
   {
   		$stanovi=Stan::all();
   		
   		foreach ($stanovi as $s) 
        {
            $minCene[$s->id] = Cenovnik::minCena($s->id);
        }

   		return view('admin.stanovi',compact('stanovi','minCene'));
   }


   public function stan($id)
   {
        $this->clearTempDir();

   		$stan = Stan::stan($id);

        $funkcije = Funkcije::all()->toArray();

    	foreach (FunkcijaStan::sveZaStan($stan->id) as $f)
    	{
    		$stan_funkcije[$f->id_funkcija] = Funkcije::funckija($f->id_funkcija);
    	}

        $osobe = [];

    	if ($stan->tip_cene == 'niz')
    	{
            $cenovnik = Cenovnik::dohvatiCene($id);

            $noci = [];
            $cene = [];

            foreach($cenovnik as $c){
                $noci[] = $c->noci;
                $cene[] = $c->cena;
            }
    	}
    	else
    	{
            $noci = [];

            $cene = [];

            $cenovnik = Cenovnik::dohvatiCene($id);

            $i = 0;
            $last_noci= -1;

            foreach($cenovnik as $c){
                if($last_noci != $c->noci){
                    $last_noci = $c->noci;
                    $i++;
                    $osobe[$i] = [];
                    $cene[$i] = [];
                    $noci[] = $c->noci;
                }

                $cene[$i][] = $c->cena;

                $osobe[$i][] = $c->osoba;
            }
    	}

    	$directory_stan = 'images/stanovi/' . $id;
    	$slike = [];

       foreach (scandir($directory_stan) as $file) {
           if ('.' === $file) continue;
           if ('..' === $file) continue;
           if ('glavna.jpg' === $file) continue;

           $slike[] = $file;
       }

       return view('admin.stan',compact('id', 'funkcije', 'stan_funkcije', 'stan', 'slike', 'noci', 'cene', 'osobe'));
   }

   private function clearTempDir(){
       $directoryPath = 'images/stanovi/temp';
       File::deleteDirectory($directoryPath);

       File::makeDirectory($directoryPath);
   }

   public function dodaj_stan()
   {
        $this->clearTempDir();

        $funkcije = Funkcije::all()->toArray();

        $id = -1; // za dodaj stan id je -1
        return view('admin.stan', compact('funkcije', 'id'));
   }

   //u php.ini upload max filesize!!!

    // ne brisati
    public function uploadImage(Request $request){
       $image = $_FILES['files'];

       $directoryPath = 'images/stanovi/temp';

       //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);

       File::move($image['tmp_name'][0], $directoryPath . '/' . $image['name'][0]);
       
       chmod($directoryPath . '/' . $image['name'][0], 0644);
    }

    public function removeImage(){
        $filename = $_POST['file'];

        $directoryPath = 'images/stanovi/temp';

        File::delete($directoryPath . '/' . $filename);
    }

    private function unesiFunkcijeCenovnik($idStana, $f, $tipCene){
        FunkcijaStan::insert($f, Funkcije::all()->toArray(), $idStana);

        if($tipCene == 'niz'){
            $noci = $_POST['arr-price-nights'];
            $cene = $_POST['arr-price-vals'];

            for($i = 0; $i < sizeof($noci); $i++){
                Cenovnik::insert($idStana, $noci[$i], $cene[$i], 0);
            }
        }
        else{
            $noci = $_POST['tab-price-nights'];

            $sve_osobe = $_POST['tab-price-people'];
            $sve_cene = $_POST['tab-price-vals'];

            for($i = 1; $i <= sizeof($noci); $i++){
                $osobe = $sve_osobe[$i];
                $cene = $sve_cene[$i];

                for($j = 0; $j < sizeof($osobe); $j++){
                    $osoba = $osobe[$j];
                    $cena = $cene[$j];

                    Cenovnik::insert($idStana, $noci[$i - 1], $cena, $osoba);
                }
            }

        }
    }

    private function sacuvajGlavnuSliku($idStana, $main_image){
        $directory_main = 'images/stanovi/' . $idStana;
        $main_name = 'glavna.jpg';

        $main_image->move($directory_main, $main_name);
        chmod($directory_main . '/' . $main_name, 0644);
    }

    private function sacuvajDodatneSlike($idStana){
        $directory_main = 'images/stanovi/' . $idStana;
        $directory_temp = 'images/stanovi/temp';

        foreach (scandir($directory_temp) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            File::move($directory_temp . '/' . $file, $directory_main . '/' . $file);
        }
    }

    public function sacuvajStan(Request $request){
        $main_image = $request->file('main_image');

        $naziv = $_POST['property-name'];
        $opis = $_POST['property-desc'];
        $opis_eng = $_POST['property-desc-en'];
        $adresa = $_POST['property-address'];
        $lokacija = $_POST['property-location'];
        $x = $_POST['property-N-coord'];
        $y = $_POST['property-E-coord'];
        $kvadratura = $_POST['property-sqm'];
        $sobe = $_POST['property-rooms'];
        $kapacitet = $_POST['property-max-people'];
        $kreveta = $_POST['property-beds'];
        $sprat = $_POST['property-floor'];
        $video = $_POST['property-video'];
        $f = $_POST['f'];

        $tipCene = $_POST['property-price'];

        $idStana = Stan::insert($naziv, $opis, $opis_eng, $adresa, $lokacija, $x, $y, $kvadratura, $sobe, $kapacitet, $kreveta, $sprat, $video, $tipCene);

        $this->unesiFunkcijeCenovnik($idStana, $f, $tipCene);

        $this->sacuvajGlavnuSliku($idStana, $main_image);

        $this->sacuvajDodatneSlike($idStana);

        return redirect('/admin/stanovi');
    }

    private function brisiZavisneTabele($idStana){
        FunkcijaStan::brisi($idStana);
        Cenovnik::brisi($idStana);
    }

    public function sacuvajIzmenjenStan(Request $request){
        $naziv = $_POST['property-name'];
        $opis = $_POST['property-desc'];
        $opis_eng = $_POST['property-desc-en'];
        $adresa = $_POST['property-address'];
        $lokacija = $_POST['property-location'];
        $x = $_POST['property-N-coord'];
        $y = $_POST['property-E-coord'];
        $kvadratura = $_POST['property-sqm'];
        $sobe = $_POST['property-rooms'];
        $kapacitet = $_POST['property-max-people'];
        $kreveta = $_POST['property-beds'];
        $sprat = $_POST['property-floor'];
        $video = $_POST['property-video'];
        $f = $_POST['f'];

        $tipCene = $_POST['property-price'];

        $id = $_POST['id'];

        $this->brisiZavisneTabele($id);

        $stan = Stan::stan($id);

        $stan->azuriraj($naziv, $opis, $opis_eng, $adresa, $lokacija, $x, $y, $kvadratura, $sobe, $kapacitet, $kreveta, $sprat, $video, $tipCene);

        $this->unesiFunkcijeCenovnik($id, $f, $tipCene);

        if($_FILES['main_image']['name'] != ''){
            $main_image = $request->file('main_image');
            $this->sacuvajGlavnuSliku($id, $main_image);
        }

        $this->sacuvajDodatneSlike($id);

        $main_directory = 'images/stanovi/' . $id;

        if(isset($_POST['toDel'])){
            $toDel = $_POST['toDel'];

            foreach($toDel as $file){
                File::delete($main_directory . '/' . $file);
            }
        }

        return redirect('/admin/stanovi');
    }

    public function brisiStan($idStana){
        Stan::brisi($idStana);
        return redirect('/admin/stanovi');
    }

    public function rezervacije(){
       $stanovi = Stan::all();

       $rezervacije = [];

       foreach($stanovi as $stan){
           $rezervacije[$stan->id] = Rezervacija::zaStan($stan->id);
       }

       return view('admin.kalendar', compact('stanovi', 'rezervacije'));
    }

    public function rezervacija($id){
        $stanovi = Stan::all();

        $rezervacije = [];

        foreach($stanovi as $stan){
            $rezervacije[$stan->id] = Rezervacija::zaStan($stan->id);
        }

        $rezervacija = null;

        if($id > 0){
            $rezervacija = Rezervacija::poId($id);
        }

        return view('admin.rezervacija', compact('stanovi', 'rezervacije', 'id', 'rezervacija'));
    }

    public function sacuvajRezervaciju(){
        $ime = $_POST['reservation-name'];
        $telefon = $_POST['reservation-phone'];
        $mail = $_POST['reservation-mail'];

        $napomena_klijent = '';
        if(isset( $_POST['reservation-note-client'])){
            $napomena_klijent =$_POST['reservation-note-client'];
        }

        $napomena_admin = '';

        if(isset( $_POST['reservation-note-admin'])){
            $napomena_admin =$_POST['reservation-note-admin'];
        }

        $br_dana = $_POST['reservation-days'];
        $br_osoba = $_POST['reservation-people'];
        $br_dece = $_POST['reservation-children'];
        $id_stan = $_POST['reservation-select-flat'];

        $dolazak = $_POST['reservation-date-start'];
        $odlazak = $_POST['reservation-date-end'];
        $cena = $_POST['reservation-price'];

        $rezervacija = Rezervacija::insert($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena);

        return redirect('/admin/rezervacije');
    }

    public function sacuvajIzmenjenuRezervaciju($id){
        $ime = $_POST['reservation-name'];
        $telefon = $_POST['reservation-phone'];
        $mail = $_POST['reservation-mail'];

        $napomena_klijent = '';
        if(isset( $_POST['reservation-note-client'])){
            $napomena_klijent =$_POST['reservation-note-client'];
        }

        $napomena_admin = '';

        if(isset( $_POST['reservation-note-admin'])){
            $napomena_admin =$_POST['reservation-note-admin'];
        }


        $br_dana = $_POST['reservation-days'];
        $br_osoba = $_POST['reservation-people'];
        $br_dece = $_POST['reservation-children'];
        $id_stan = $_POST['reservation-select-flat'];

        $dolazak = $_POST['reservation-date-start'];
        $odlazak = $_POST['reservation-date-end'];
        $cena = $_POST['reservation-price'];

        $rezervacija = Rezervacija::poId($id);

        $rezervacija->azuriraj($ime, $telefon, $mail, $napomena_klijent, $napomena_admin, $br_dana, $br_osoba, $br_dece, $id_stan, $dolazak, $odlazak, $cena);

        return redirect('/admin/rezervacije');
    }

    public function brisiRezervaciju($id){
       Rezervacija::brisiPoId($id);

       return redirect('/admin/rezervacije');
    }
}
