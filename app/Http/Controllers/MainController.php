<?php

namespace App\Http\Controllers;

use App\FunkcijaStan;
use App\Stan;
use App\Cenovnik;
use Illuminate\Http\Request;
use Mail;
use Auth;
use App\Http\Requests;
use App\User;
use Redirect;

use Illuminate\Support\Facades\Hash;

class MainController extends Controller
{
    public function naslovna()
    {
    	$stanovi=Stan::all();

    	$cene= [];
        $slike = [];

    	foreach($stanovi as $stan) {
            $directory_stan = 'images/stanovi/' . $stan->id;

            foreach (scandir($directory_stan) as $file) {
                if ('.' === $file) continue;
                if ('..' === $file) continue;
                if ('glavna.jpg' === $file) continue;

                $slike[$stan->id][] = $file;
            }

        }
    	$imaParking = [];

    	foreach ($stanovi as $s) 
    	{
    		$cene[$s->id] = Cenovnik::minCena($s->id);

            $imaParking[$s->id] = false;

    		if(count(FunkcijaStan::imaParking($s->id)) > 0){
                $imaParking[$s->id] = true;
            }
    	}


        return view ('welcome',compact('stanovi','cene', 'slike', 'imaParking'));
    }


    
    public function odjavi_se()
    {
        Auth::logout();
        return Redirect::to('/');
    }


    public function kontaktiraj(Request $request)
    {

        $kontakt['ime'] = $request->input('ime');
        $kontakt['email'] = $request->input('email');
        $kontakt['telefon'] = $request->input('telefon');
        $kontakt['poruka'] = $request->input('poruka');
   

      Mail::send('mail_kontakt', $kontakt, function($message) use ($kontakt) {
         $message->to('info@trip-apartmani.com', 'Kontakt forma sajt ')->subject('Kontakt forma sajt '.$kontakt['ime']);
         $message->from('upit@trip-apartmani.com','Sajt');
        });



        return redirect('/kontakt-uspesan');
    }


    public function rezervacija(Request $request)
    {

        $kontakt['ime'] = $request->input('ime');
        $kontakt['email'] = $request->input('email');
        $kontakt['telefon'] = $request->input('telefon');
        $kontakt['napomena'] = $request->input('napomena');
        $kontakt['datum_od'] = $request->input('datum_od');
        $kontakt['datum_do'] = $request->input('datum_do');

        

      Mail::send('mail_rezervacija', $kontakt, function($message) use ($kontakt) {
         $message->to('info@trip-apartmani.com', 'REZERVACIJA ')->subject('REZERVACIJA - '.$kontakt['ime']);
         $message->from('upit@trip-apartmani.com','REZERVACIJA Sajt');
        });



        return redirect('/uspesna-rezervacija');
    }





}
