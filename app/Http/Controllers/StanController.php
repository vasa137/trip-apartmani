<?php

namespace App\Http\Controllers;

use App\Stan;
use App\Cenovnik;
use App\Funkcije;
use App\FunkcijaStan;
use App\Rezervacija;
use Illuminate\Http\Request;
use DateTime;
class StanController extends Controller
{


    public function stan(Request $request,$link,$id)
    {
    	$stan= Stan::stan($id);

        $directory_stan = 'images/stanovi/' . $id;
        $slike = [];

        $cenovnik = Cenovnik::dohvatiCene($id);


        $osobe = [];
        $noci = [];
        $cene = [];

        if ($stan->tip_cene == 'niz')
        {
            foreach($cenovnik as $c){
                $noci[] = $c->noci;
                $cene[] = $c->cena;
            }
        }
        else
        {
            $i = 0;
            $last_noci= -1;

            foreach($cenovnik as $c){
                if($last_noci != $c->noci){
                    $last_noci = $c->noci;
                    $i++;
                    $osobe[$i] = [];
                    $cene[$i] = [];
                    $noci[] = $c->noci;
                }

                $cene[$i][] = $c->cena;

                $osobe[$i][] = $c->osoba;
            }
        }

        foreach (scandir($directory_stan) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;
            if ('glavna.jpg' === $file) continue;

            $slike[] = $file;
        }

    	foreach (FunkcijaStan::sveZaStan($stan->id) as $f)
    	{
    		$funkcije[$f->id_funkcija] = Funkcije::funckija($f->id_funkcija);
    	}

    	$rezervacije = Rezervacija::zaStan($id);

    	$crveni_dani = "";

    	foreach($rezervacije as $rez) {

            $datumPocetka = date("m/d/y", strtotime($rez->dolazak));
    	    $datumKraja = date("m/d/y", strtotime($rez->odlazak));


    	    while($datumPocetka != $datumKraja) {

                if ($crveni_dani != '') {
                    $crveni_dani .= ',';
                }
                $crveni_dani .= $datumPocetka;

                $datumPocetka = date("m/d/y",strtotime($datumPocetka . '+1 days'));
            }
        }


        $minCena = Cenovnik::minCena($stan->id);

        return view ('stan',compact('stan','cene','funkcije','crveni_dani', 'slike','noci', 'cene','osobe', 'minCena'));
    }

    public function mapa_stanova()
    {
        $stanovi=Stan::all();

        $imaParking = [];

        foreach ($stanovi as $s) 
        {
            $cene[$s->id] = Cenovnik::minCena($s->id);
            $minCene[$s->id] = Cenovnik::minCena($s->id);

            $imaParking[$s->id] = false;

            if(count(FunkcijaStan::imaParking($s->id)) > 0){
                $imaParking[$s->id] = true;
            }
        }



        return view('mapa_stanova',compact('stanovi','cene', 'minCene', 'imaParking'));
    }

    public function stanovi(Request $request)
    {
        $dolazakInp = $request->input('dolazak');
        $odlazakInp = $request->input('odlazak');
        $br_osoba = $request->input('br_osoba');
        $parking = 0;

        if($br_osoba == null){
            $br_osoba = 0;
        }

        if(isset($_GET['parking'])){
            $parking = 1;
        }

        $dolazak= date("Y-m-d", strtotime(str_replace('/', '-', $dolazakInp)));
        $odlazak= date("Y-m-d", strtotime(str_replace('/', '-', $odlazakInp)));

        $termin = 'Bilo koji termin';

        if(strtotime($odlazak) - strtotime($dolazak) > 0)
        {
            $termin =   date("d.m.Y", strtotime(str_replace('/', '-', $dolazakInp))).' - ' .date("d.m.Y", strtotime(str_replace('/', '-', $odlazakInp)));

            $stanovi = Rezervacija::slobodniStanovi($dolazak, $odlazak, $br_osoba, $parking);
        }
        else
        {
            $niz_stanova = Stan::all();;
            $stanovi = [];

            foreach($niz_stanova as $stan){
                if($parking && count(FunkcijaStan::imaParking($stan->id)) == 0){
                    continue;
                } else if($br_osoba > $stan->kapacitet){
                    continue;
                }

                $stanovi[] = $stan;
            }
        }

        $imaParking = [];

        foreach ($stanovi as $s) 
        {
            $cene[$s->id] = Cenovnik::minCena($s->id);

            $imaParking[$s->id] = false;

            if(count(FunkcijaStan::imaParking($s->id)) > 0){
                $imaParking[$s->id] = true;
            }
        }

        return view('stanovi',compact('stanovi','cene','termin', 'imaParking'));
    }

}


