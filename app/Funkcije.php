<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funkcije extends Model
{
    protected $fillable = [
        'id', 'naziv','naziv_eng' ,'ikonica' 	
    ];
    protected $table = 'funkcija';
    protected $table_name = 'funkcija';


    public static function funckija($id)
    {
    	return Funkcije::where('id',$id)->first();
    }

}
